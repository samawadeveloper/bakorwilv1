@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/mastering_program/mastering_program.js') }}">
        </script>
    @endpush
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Mastering Program
                    </h2>
                    <div class="col-2 col-sm-4 col-md-2 col-xl mb-2 mt-2">
                        <a href="/tambah-pengguna" class="btn btn-info btn-square w-100" data-bs-toggle="modal"
                            data-bs-target="#program-baru">
                            Tambah Program
                        </a>
                    </div>
                </div>

            </div>

        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-body" style="padding: 0 !important">
                        <table class="table table-vcenter table-program">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th style="width:350px;">Program, Kegiatan dan Sub Kegiatan</th>
                                    <th style="width:350px;">Indikator Program, Kegiatan dan Sub Kegiatan</th>
                                    <th style="width:20px;" align="center">Target</th>
                                    <th style="text-align:center">Satuan</th>
                                    <th style="text-align:center">Pagu</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-blur fade" id="program-baru" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form class="form" id="formAddProgram">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Program Baru</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-3 align-items-end">
                            <div class="col">
                                <label class="form-label">Kode <span class="err_notif_custom err_kode"></span></label>
                                <input type="text" id="kode" class="form-control" />
                            </div>
                        </div>
                        <div>
                            <label class="form-label">Program <span class="err_notif_custom err_program"></span></label>
                            <textarea class="form-control" id="program"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="btnSubmit" class="btn btn-primary">Tambah Program</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="kegiatan-baru" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form class="form" id="formAddKegiatan">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Kegiatan Baru</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-3 align-items-end">
                            <div class="col">
                                <label class="form-label">Kode <span
                                        class="err_notif_custom err_kode_kegiatan"></span></label>
                                <input type="hidden" id="id_program" class="form-control" />
                                <input type="text" id="kode_kegiatan" class="form-control" />
                            </div>
                        </div>
                        <div>
                            <label class="form-label">Kegiatan <span class="err_notif_custom err_kegiatan"></span></label>
                            <textarea class="form-control" id="kegiatan"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="btnSubmitKegiatan" class="btn btn-primary">Tambah Kegiatan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="subkegiatan-baru" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form class="form" id="formAddSubKegiatan">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Sub Kegiatan Baru</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-3 align-items-end">
                            <div class="col">
                                <label class="form-label">Kode <span
                                        class="err_notif_custom err_kode_subkegiatan"></span></label>
                                <input type="hidden" id="id_kegiatan" class="form-control" />
                                <input type="text" id="kode_sub_kegiatan" class="form-control" />
                            </div>
                        </div>
                        <div>
                            <label class="form-label">Sub Kegiatan <span
                                    class="err_notif_custom err_subkegiatan"></span></label>
                            <textarea class="form-control" id="sub_kegiatan"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="btnSubmitSubKegiatan" class="btn btn-primary">Tambah Kegiatan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-blur fade" id="subbidang-baru" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form class="form" id="formAddSubSubKegiatan">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Sub Bidang Kegiatan Baru</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-3 align-items-end">
                            <div class="col">
                                <label class="form-label">Kode <span
                                        class="err_notif_custom err_kode_subsubkegiatan"></span></label>
                                <input type="hidden" id="id_sub_kegiatan" class="form-control" />
                                <input type="text" id="kode_sub_sub_kegiatan" class="form-control" />
                            </div>
                        </div>
                        <div>
                            <label class="form-label">Kegiatan <span
                                    class="err_notif_custom err_subsubkegiatan"></span></label>
                            <textarea class="form-control" id="sub_sub_kegiatan"></textarea>
                        </div>
                        <div>
                            <label class="form-label">Sub Bidang <span
                                    class="err_notif_custom err_subbidang"></span></label>
                            <select name="sub_bidang" class="form-select" id="sub_bidang">
                                @foreach ($subbid as $itemsubbid)
                                    <option value="{{$itemsubbid->id}}">{{$itemsubbid->sub_bid}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="btnSubmitSubSubKegiatan" class="btn btn-primary">Tambah Kegiatan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="indikator-program" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form class="form" id="formAddIndikatorProgram">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Indikator Program</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div>
                            <label class="form-label">Indikator <span class="err_notif_custom err_indikator_program"></span></label>
                            <textarea class="form-control" id="indikator_program"></textarea>
                            <input type="hidden" id="id_program_indikator" class="form-control" />
                        </div>
                        <div class="mt-2">
                            <label class="form-label">Satuan <span class="err_notif_custom err_satuan"></span></label>
                            <input type="text" class="form-control" id="satuan_program">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="btnSubmitIndikatorProgram" class="btn btn-primary">Tambah Kegiatan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="indikator-kegiatan" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form class="form" id="formAddIndikatorKegiatan">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Indikator Kegiatan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div>
                            <label class="form-label">Indikator <span class="err_notif_custom err_indikator_kegiatan"></span></label>
                            <textarea class="form-control" id="indikator_kegiatan"></textarea>
                            <input type="hidden" id="id_kegiatan_indikator" class="form-control" />
                        </div>
                        <div class="mt-2">
                            <label class="form-label">Satuan <span class="err_notif_custom err_satuan_kegiatan"></span></label>
                            <input type="text" class="form-control" id="satuan_kegiatan">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="btnSubmitIndikatorKegiatan" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-blur fade" id="indikator-subkegiatan" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form class="form" id="formAddIndikatorSubKegiatan">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Indikator Sub Kegiatan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div>
                            <label class="form-label">Indikator <span class="err_notif_custom err_indikator_sub_kegiatan"></span></label>
                            <textarea class="form-control" id="indikator_sub_kegiatan"></textarea>
                            <input type="hidden" id="id_sub_kegiatan_indikator" class="form-control" />
                        </div>
                        <div class="mt-2">
                            <label class="form-label">Satuan <span class="err_notif_custom err_satuan_sub_kegiatan"></span></label>
                            <input type="text" class="form-control" id="satuan_sub_kegiatan">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="btnSubmitIndikatorSubKegiatan" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-blur fade" id="indikator-subbidang" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form class="form" id="formAddIndikatorSubBidang">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Indikator Sub Bidang</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div>
                            <label class="form-label">Indikator <span class="err_notif_custom err_indikator_sub_bidang"></span></label>
                            <textarea class="form-control" id="indikator_sub_bidang"></textarea>
                            <input type="hidden" id="id_sub_bidang_indikator" class="form-control" />
                        </div>
                        <div class="mt-2">
                            <label class="form-label">Satuan <span class="err_notif_custom err_satuan_sub_bidang"></span></label>
                            <input type="text" class="form-control" id="satuan_sub_bidang">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="btnSubmitIndikatorSubBidang" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="target" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form class="form" id="formTarget" action="">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Target</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div>
                            <label class="form-label">Tahun <span class="err_notif_custom err_tahun"></span></label>
                            <input type="text" class="form-control" id="tahun">
                            <input type="hidden" id="id_indikator" class="form-control" />
                        </div>
                        <div class="mt-2">
                            <label class="form-label">Target <span class="err_notif_custom err_target"></span></label>
                            <input type="text" class="form-control" id="target_modal">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="btnSubmitTarget" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="pagu" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form class="form" id="formPagu">
                    <div class="modal-header">
                        <h5 class="modal-title">Atur Pagu</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div>
                            <label class="form-label">Pagu <span class="err_notif_custom err_pagu"></span></label>
                            <input type="text" class="form-control" id="pagu_txt">
                            <input type="hidden" id="id_sub_bidang" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="btnSubmitPagu" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
