@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/laporan/laporan_kegiatan.js') }}">
        </script>
    @endpush

    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Laporan Kegiatan
                    </h2>
                </div>
                <div class="col-auto ms-auto d-print-none">
                    <div class="btn-list">
                        <a href="/cetak-laporan-kegiatan">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M17 17h2a2 2 0 0 0 2 -2v-4a2 2 0 0 0 -2 -2h-14a2 2 0 0 0 -2 2v4a2 2 0 0 0 2 2h2" /><path d="M17 9v-4a2 2 0 0 0 -2 -2h-6a2 2 0 0 0 -2 2v4" /><rect x="7" y="13" width="10" height="8" rx="2" /></svg> Cetak
                        </a>
                    </div>
                </div>

            </div>

        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-body" style="padding: 0 !important">
                        <table class="table table-vcenter table-program">
                            <thead>
                                <tr>
                                    <th style="width:100px;">Kode</th>
                                    <th style="width:300px;">Program, Kegiatan dan Sub Kegiatan</th>
                                    <th style="width:300px;">Indikator</th>
                                    <th style="width:150px;text-align:center">Realisasi</th>
                                    <th style="width:200px;">Deskripsi Realisasi</th>
                                    <th style="text-align:center">Pagu</th>
                                    <th style="text-align:center">Realisasi Anggaran</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection