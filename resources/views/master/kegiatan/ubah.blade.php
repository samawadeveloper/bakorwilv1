@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/master/kegiatan/ubah.js') }}"></script>
    @endpush
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Kegiatan - Ubah
                    </h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Ubah Kegiatan</h3>
                    </div>
                    <div class="card-body">
                        <form class="form" id="formUbah">
                            <div class="form-group mb-3">
                                <label class="form-label">Pilih Program <span class="err_notif_custom err_program"></span> </label>
                                <div class="col-md-8">
                                    <select name="program" class="form-select" id="program">
                                        <option value="">Pilih Kode Program</option>                                        
                                        @foreach ($program as $programEach)
                                            <option @if($data->id_program == $programEach->id) selected @endif value="{{$programEach->id}}">{{$programEach->kode}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Kode <span class="err_notif_custom err_kode"></span> </label>
                                <div class="col-md-8">
                                    <input type="hidden" value="{{$data->id}}" class="form-control" name="id" id="id" placeholder="Nama">
                                    <input type="text" class="form-control" value="{{$data->kode}}" name="kode" id="kode" placeholder="Nama">
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label"> Kegiatan <span class="err_notif_custom err_kegiatan"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control"  value="{{$data->kegiatan}}" name="kegiatan" id="kegiatan"
                                        placeholder="Kegiatan">
                                </div>
                            </div>
                            
                            <div class="form-footer">
                                <a href="/kegiatan" class="btn btn-default">Batal</a>
                                <button type="submit" id="btnSubmit" class="btn btn-primary">Tambah</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
