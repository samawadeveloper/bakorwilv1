@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/master/subsubkegiatan/ubah.js') }}"></script>
    @endpush
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Sub Sub Kegiatan - Ubah
                    </h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Ubah Sub Sub Kegiatan</h3>
                    </div>
                    <div class="card-body">
                        <form class="form" id="formUbah">
                            <div class="form-group mb-3">
                                <label class="form-label">Pilih Program <span class="err_notif_custom err_program"></span> </label>
                                <div class="col-md-8">
                                    <select name="program" class="form-select" id="program">
                                        <option value="">Pilih Kode Program</option>                                        
                                        @foreach ($program as $programEach)
                                            <option @if($data->id_program == $programEach->id) selected @endif value="{{$programEach->id}}">{{$programEach->kode}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Pilih Kegiatan <span class="err_notif_custom err_kegiatan"></span> </label>
                                <div class="col-md-8">
                                    <select name="kegiatan" class="form-select" id="kegiatan">
                                        <option value="">Pilih Kode Kegiatan</option>      
                                        @foreach ($kegiatan as $kegiatanEach)
                                            <option @if($data->id_kegiatan == $kegiatanEach->id) selected @endif value="{{$kegiatanEach->id}}">{{$kegiatanEach->kode}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Pilih Sub Kegiatan <span class="err_notif_custom err_subkegiatan"></span> </label>
                                <div class="col-md-8">
                                    <select name="kegiatan" class="form-select" id="subkegiatan">
                                        <option value="">Pilih Kode Sub Kegiatan</option>      
                                        @foreach ($subkegiatan as $subkegiatanEach)
                                            <option @if($data->id_sub_kegiatan == $subkegiatanEach->id) selected @endif value="{{$subkegiatanEach->id}}">{{$subkegiatanEach->kode}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Kode Sub Sub Kegiatan <span class="err_notif_custom err_kode"></span> </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="kode" value="{{$data->kode}}" id="kode" placeholder="Kode">
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label"> Sub Sub Kegiatan <span class="err_notif_custom err_subsubkegiatan"></span></label>
                                <div class="col-md-8">
                                    <input type="text" value="{{$data->sub_bid_kegiatan}}" class="form-control" name="subsubkegiatan" id="subsubkegiatan" placeholder="Sub Sub Kegiatan">
                                    <input type="hidden" value="{{$data->id}}" class="form-control" name="id" id="id">
                                </div>
                            </div>

                            <div class="form-group mb-3">
                                <label class="form-label">Pilih Sub Bidang <span class="err_notif_custom err_subbidang"></span> </label>
                                <div class="col-md-8">
                                    <select name="subbidang" class="form-select" id="subbidang">
                                        <option value="">Pilih Sub Bidang</option>      
                                        @foreach ($subbidang as $subbidangEach)
                                            <option @if($data->id_sub_bidang == $subbidangEach->id) selected @endif value="{{$subbidangEach->id}}">{{$subbidangEach->sub_bid}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-footer">
                                <a href="/sub-sub-kegiatan" class="btn btn-default">Batal</a>
                                <button type="submit" id="btnSubmit" class="btn btn-primary">Tambah</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
