@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/master/subsubkegiatan/tambah_indikator.js') }}"></script>
    @endpush
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Sub Sub Kegiatan Indikator - Tambah Baru
                    </h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Tambah Indikator Sub Sub Kegiatan</h3>
                    </div>
                    <div class="card-body">
                        <form class="form" id="formAdd">
                            <div class="form-group mb-3">
                                <label class="form-label">Indikator<span class="err_notif_custom err_indikator"></span> </label>
                                <div class="col-md-8">
                                    <textarea class="form-control" name="indikator" id="indikator" placeholder="Indikator"></textarea>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label"> Satuan <span class="err_notif_custom err_satuan"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="satuan" id="satuan" placeholder="Satuan">
                                    <input type="hidden" class="form-control" name="id" id="id" value="{{$id}}">
                                </div>
                            </div>
                            
                            <div class="form-footer">
                                <a href="/indikator-sub-sub-kegiatan/{{$id}}" class="btn btn-default">Batal</a>
                                <button type="submit" id="btnSubmit" class="btn btn-primary">Tambah</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
