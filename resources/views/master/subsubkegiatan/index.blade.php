@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/master/subsubkegiatan/index.js') }}"></script>
    @endpush
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Sub Sub Kegiatan
                    </h2>
                    <div class="col-2 col-sm-4 col-md-2 col-xl mb-2 mt-2">
                        <a href="/tambah-sub-sub-kegiatan" class="btn btn-primary btn-square w-100">
                            Tambah Sub Sub Kegiatan
                        </a>
                    </div>
                </div>

            </div>

        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Sub Sub Kegiatan</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered zero-configuration dtable-sub-sub-kegiatan">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Sub Sub Kegiatan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
