@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/master/program/indikator.js') }}"></script>
    @endpush
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Indikator Program
                    </h2>
                    <div class="row g-2 align-items-center mb-n3">
                        <div class="col-1 mb-2 mt-2">
                            <a href="/program" class="btn btn-default">Kembali</a>
                        </div>
                        <div class="col-2 mb-2 mt-2">
                            <a href="/tambah-indikator-program/{{$id}}" class="btn btn-primary btn-square">
                                Tambah Indikator
                            </a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Indikator  {{$kode}} / {{$program}} </h3>
                        <span id="program-id" style="display: none">{{$id}}</span>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered zero-configuration dtable-indikator">
                            <thead>
                                <tr>
                                    <th>Indikator</th>
                                    <th>Satuan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
