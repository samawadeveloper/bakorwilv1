@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/master/program/tambah.js') }}"></script>
    @endpush
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Program - Tambah Baru
                    </h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Tambah Program</h3>
                    </div>
                    <div class="card-body">
                        <form class="form" id="formAdd">
                            <div class="form-group mb-3">
                                <label class="form-label">Kode <span class="err_notif_custom err_kode"></span> </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="kode" id="kode" placeholder="Kode">
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label"> Program <span class="err_notif_custom err_program"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="program" id="program"
                                        placeholder="Program">
                                </div>
                            </div>
                            
                            <div class="form-footer">
                                <a href="/program" class="btn btn-default">Batal</a>
                                <button type="submit" id="btnSubmit" class="btn btn-primary">Tambah</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
