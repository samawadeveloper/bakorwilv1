@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/master/program/ubah.js') }}"></script>
    @endpush
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Program - Ubah
                    </h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Ubah Program</h3>
                    </div>
                    <div class="card-body">
                        <form class="form" id="formUbah">
                            <div class="form-group mb-3">
                                <label class="form-label">Kode <span class="err_notif_custom err_kode"></span> </label>
                                <div class="col-md-8">
                                    <input type="hidden" value="{{$data->id}}" class="form-control" name="id" id="id" placeholder="Nama">
                                    <input type="text" class="form-control" value="{{$data->kode}}" name="kode" id="kode" placeholder="Nama">
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label"> Program <span class="err_notif_custom err_program"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control"  value="{{$data->program}}" name="program" id="program"
                                        placeholder="Program">
                                </div>
                            </div>
                            
                            <div class="form-footer">
                                <a href="/program" class="btn btn-default">Batal</a>
                                <button type="submit" id="btnSubmit" class="btn btn-primary">Tambah</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
