@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/master/subkegiatan/tambah.js') }}"></script>
    @endpush
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Sub Kegiatan - Tambah Baru
                    </h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Tambah Sub Kegiatan</h3>
                    </div>
                    <div class="card-body">
                        <form class="form" id="formAdd">
                            <div class="form-group mb-3">
                                <label class="form-label">Pilih Program <span class="err_notif_custom err_program"></span> </label>
                                <div class="col-md-8">
                                    <select name="program" class="form-select" id="program">
                                        <option value="">Pilih Kode Program</option>                                        
                                        @foreach ($program as $programEach)
                                            <option value="{{$programEach->id}}">{{$programEach->kode}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Pilih Kegiatan <span class="err_notif_custom err_kegiatan"></span> </label>
                                <div class="col-md-8">
                                    <select name="kegiatan" class="form-select" id="kegiatan">
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Kode Kegiatan <span class="err_notif_custom err_kode"></span> </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="kode" id="kode" placeholder="Kode">
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label"> Sub Kegiatan <span class="err_notif_custom err_subkegiatan"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="kegiatan" id="subkegiatan"
                                        placeholder="Sub Kegiatan">
                                </div>
                            </div>
                            
                            <div class="form-footer">
                                <a href="/sub-kegiatan" class="btn btn-default">Batal</a>
                                <button type="submit" id="btnSubmit" class="btn btn-primary">Tambah</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
