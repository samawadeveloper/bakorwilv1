<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-alpha.24
* @link https://tabler.io
* Copyright 2018-2021 The Tabler Authors
* Copyright 2018-2021 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <meta name="author" content="SAMAWA CREATIVE">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" href="{{asset('assets/img/fav.png')}}" type="image/x-icon">
    <title>Halaman Masuk</title>
    <!-- CSS files -->
    <link href="{{asset('assets/css/tabler.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/tabler-flags.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/tabler-payments.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/tabler-vendors.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/demo.min.css')}}" rel="stylesheet"/>
  </head>
  <body class="antialiased border-top-wide border-primary d-flex flex-column">
  
    @yield('content')

    <!-- Libs JS -->
    <!-- Tabler Core -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="{{asset('assets/js/tabler.min.js')}}"></script>
    <script src="{{asset('assets/js/_bwkauth.js')}}"></script>
  </body>
</html>