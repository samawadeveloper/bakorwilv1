@extends('auth.auth')

@section('content')
    <div class="flex-fill d-flex flex-column justify-content-center py-4">
        <div class="container-tight py-6" style="max-width: 25rem !important;">
            <div class="text-center mb-4">
                <a href="."><img src="{{ asset('assets/img/logobakorwilmalang_sticky.png') }}" height="80" alt=""></a>
            </div>
            <form class="card card-md" id="formLoginAdmin">
                <div class="card-body">
                    <h2 class="card-title text-center mb-4">Masuk ke Halaman</h2>
                    <div class="mb-3">
                        <label class="form-label">Username</label>
                        <input type="email" id="username" class="form-control" placeholder="Masukan Username">
                    </div>
                    <div class="mb-2">
                        <label class="form-label">
                            Password
                        </label>
                        <div class="input-group input-group-flat">
                            <input type="password" id="password" class="form-control" placeholder="Password"
                                autocomplete="off">
                            <span class="input-group-text">
                                <a href="#" class="link-secondary" title="Show password" data-bs-toggle="tooltip"><svg
                                        xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                                        viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                        stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                        <circle cx="12" cy="12" r="2" />
                                        <path
                                            d="M22 12c-2.667 4.667 -6 7 -10 7s-7.333 -2.333 -10 -7c2.667 -4.667 6 -7 10 -7s7.333 2.333 10 7" />
                                    </svg>
                                </a>
                            </span>
                        </div>
                    </div>
                    <div class="mb-2">
                        <label class="form-check">
                            <input type="checkbox" class="form-check-input" />
                            <span class="form-check-label">Ingat password</span>
                        </label>
                    </div>
                    <div class="form-footer">
                        <p class="feedback-login"></p>
                        <button type="submit" id="btn-login" class="btn btn-primary w-100">Masuk</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
