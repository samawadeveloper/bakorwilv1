@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/sub_bidang/sub_bidang.js') }}"></script>
    @endpush
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Sub Bidang
                    </h2>
                    <div class="col-2 col-sm-4 col-md-2 col-xl mb-2 mt-2">
                        <a href="/tambah-sub-bidang" class="btn btn-primary btn-square w-100">
                            Tambah
                        </a>
                    </div>
                </div>

            </div>

        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Sub Bidang</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered zero-configuration dtable-sub-bidang">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Sub Bidang</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
