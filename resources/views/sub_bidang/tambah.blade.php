@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/sub_bidang/sub_bidang_tambah.js') }}"></script>
    @endpush
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Sub Bidang - Tambah Baru
                    </h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Tambah Sub Bidang</h3>
                    </div>
                    <div class="card-body">
                        <form class="form" id="formAddSubBidang">
                            <div class="form-group mb-3">
                                <label class="form-label">Sub Bidang <span class="err_notif_custom err_sub_bidang"></span> </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="sub_bid" id="sub_bid" placeholder="Sub Bidang">
                                </div>
                            </div>
                            <div class="form-footer">
                                <a href="/sub-bidang" class="btn btn-default">Kembali</a>
                                <button type="submit" id="btnSubmit" class="btn btn-primary">Tambah Data</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
