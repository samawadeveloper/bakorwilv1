@extends('app')

@section('content')
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Laporan Kinerja Baru - Sasaran Kegiatan
                    </h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Sasaran Kegiatan</h3>
                    </div>
                    <div class="card-body">
                        
                            <div class="form-group mb-3">
                                <label class="form-label">Sasaran Kegiatan</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" name="sasaran_kegiatan" id="sasaran_kegiatan" 
                                        placeholder="Sasaran Kegiatan"></textarea>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Indikator Kinerja</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" name="indikator_kinerja" id="indikator_kinerja"
                                        placeholder="Indikator Kinerja"></textarea>
                                </div>
                            </div>
                            <div class="form-group mb-3" style="border-bottom:solid 1px #e3e4e8;;padding-bottom:25px">
                                <label class="form-label">Target (%)</label>
                                <div class="col-md-3">
                                    <input type="number" class="form-control"
                                        placeholder="0" name="target" id="target">
                                </div>
                            </div>
                            <div class="form-group mb-3" style="border-bottom:solid 1px #e3e4e8;;padding-bottom:25px">
                                <label class="form-label">Tahun</label>
                                <div class="col-md-3">
                                    <input type="number" class="form-control"
                                        placeholder="{{date('Y')}}" name="target" id="target">
                                </div>
                            </div>
                            <div class="form-footer">
                                <a href="" class="btn btn-default">Batalkan</a>
                                <a href="/laporan-kinerja-baru/kegiatan" class="btn btn-primary">Lanjutkan</a>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
