@extends('app')

@section('content')
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Laporan Kinerja Baru - Pembagian Kegiatan
                    </h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Pembagian Kegiatan</h3>
                    </div>
                    <div class="card-body">
                        <div class="col-12">
                            <div class="card">
                                <div class="table-responsive">
                                    <table class="table table-vcenter card-table table-striped">
                                        <tbody>
                                            <tr>
                                                <td>3.14.21.003</td>
                                                <td class="text-muted" colspan="2">
                                                    Sinkronisasi Penyelengaraan Pemerintahan Bidang Perpustakaan, Kearsipan,
                                                    Statistik, Persandian dan Tugas Pembantuan
                                                </td>
                                            </tr>
                                            @for($i=1; $i<=12; $i++)
                                            <tr>
                                                <td></td>
                                                <td>Bulan - {{$i}}</td>
                                                <td>
                                                    <select name="" id="">
                                                        <option value="">Sub Bidang Pemerintahan I</option>
                                                        <option value="">Sub Bidang Pemerintahan II</option>
                                                        <option value="">Sub Bidang Pembangunan Ekonomi I</option>
                                                        <option value="">Sub Bidang Pembangunan Ekonomi II</option>
                                                        <option value="">Sub Bidang Kemasyarakatan I</option>
                                                        <option value="">Sub Bidang Kemasyarakatan II</option>
                                                        <option value="">Sub Bidang Sarana dan Prasarana I</option>
                                                        <option value="">Sub Bidang Sarana dan Prasarana II</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            @endfor
                                            <tr>
                                                <td>3.14.21.004</td>
                                                <td class="text-muted" colspan="2">
                                                    Sinkronisasi, Monitoring, dan Evaluasi Pelaksanaan Pembangunan Bidang Pemerintahan
                                                </td>
                                            </tr>
                                            @for($y=1; $y<=12; $y++)
                                            <tr>
                                                <td></td>
                                                <td>Bulan - {{$y}}</td>
                                                <td>
                                                    <select name="" id="">
                                                        <option value="">Sub Bidang Pemerintahan I</option>
                                                        <option value="">Sub Bidang Pemerintahan II</option>
                                                        <option value="">Sub Bidang Pembangunan Ekonomi I</option>
                                                        <option value="">Sub Bidang Pembangunan Ekonomi II</option>
                                                        <option value="">Sub Bidang Kemasyarakatan I</option>
                                                        <option value="">Sub Bidang Kemasyarakatan II</option>
                                                        <option value="">Sub Bidang Sarana dan Prasarana I</option>
                                                        <option value="">Sub Bidang Sarana dan Prasarana II</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            @endfor
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-footer">
                            <a href="/laporan-kinerja-baru/sub-kegiatan" class="btn btn-default">Sebelumnya</a>
                            <a href="/laporan-kinerja-baru/sub-kegiatan" class="btn btn-primary">Lanjutkan</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
