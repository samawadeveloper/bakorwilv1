@extends('app')

@section('content')
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Laporan Kinerja Baru - Sub Kegiatan
                    </h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Sub Kegiatan</h3>
                    </div>
                    <div class="card-body">
                        
                            <div class="form-group mb-3">
                                <label class="form-label">Kode</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="kode" id="kode" 
                                        placeholder="Kode">
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Sub Kegiatan</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" name="sub_kegiatan" id="sub_kegiatan"
                                        placeholder="Sub Kegiatan"></textarea>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Target</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="target" id="target"
                                        placeholder="Target">
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Pagu (Rp)</label>
                                <div class="col-md-3">
                                    <input type="number" class="form-control"
                                        placeholder="0" name="pagu" id="pagu">
                                </div>
                            </div>
                            <div class="form-group mb-3" style="border-bottom:solid 1px #e3e4e8;padding-bottom:25px">
                                <label class="form-label">Prosentase (Target 90%)</label>
                                <div class="col-md-3">
                                    <input type="number" class="form-control"
                                        placeholder="0" name="prosentase" id="prosentase">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-vcenter card-table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Kode</th>
                                                    <th>Sub Kegiatan</th>
                                                    <th>Target</th>
                                                    <th>Pagu</th>
                                                    <th>Prosentase</th>
                                                    <th class="w-1"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>3.14.21.003</td>
                                                    <td class="text-muted">
                                                        Sinkronisasi Penyelengaraan Pemerintahan Bidang Perpustakaan, Kearsipan, Statistik, Persandian dan Tugas Pembantuan
                                                    </td>
                                                    <td>1 Sidang</td>
                                                    <td class="text-muted"><a href="#" class="text-reset">200.000.000</a></td>
                                                    <td class="text-muted">
                                                        45%
                                                    </td>
                                                    <td>
                                                        <a href="#">Edit</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3.14.21.004</td>
                                                    <td class="text-muted">
                                                        Sinkronisasi, Monitoring, dan Evaluasi Pelaksanaan Pembangunan Bidang Pemerintahan
                                                    </td>
                                                    <td>1 Rekomendasi</td>
                                                    <td class="text-muted"><a href="#" class="text-reset">240.000.000</a></td>
                                                    <td class="text-muted">
                                                        45%
                                                    </td>
                                                    <td>
                                                        <a href="#">Edit</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="form-footer">
                                <a href="/laporan-kinerja-baru/kegiatan" class="btn btn-default">Sebelumnya</a>
                                <a href="/laporan-kinerja-baru/pembagian-kegiatan" class="btn btn-primary">Lanjutkan</a>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
