@extends('app')

@section('content')
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Laporan Kinerja Baru - Kegiatan
                    </h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Kegiatan</h3>
                    </div>
                    <div class="card-body">
                        
                            <div class="form-group mb-3">
                                <label class="form-label">Kode</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="kode" id="kode" 
                                        placeholder="Kode">
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Nama Kegiatan</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="kegiatan" id="kegiatan"
                                        placeholder="Indikator Kinerja">
                                </div>
                            </div>
                            <div class="form-group mb-3" style="border-bottom:solid 1px #e3e4e8;padding-bottom:25px">
                                <label class="form-label">Pagu (Rp)</label>
                                <div class="col-md-3">
                                    <input type="number" class="form-control"
                                        placeholder="0" name="pagu" id="pagu">
                                </div>
                            </div>
                            
                            <div class="form-footer">
                                <a href="/laporan-kinerja-baru/sasaran" class="btn btn-default">Sebelumnya</a>
                                <a href="/laporan-kinerja-baru/sub-kegiatan" class="btn btn-primary">Lanjutkan</a>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
