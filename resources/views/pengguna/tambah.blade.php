@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/pengguna/pengguna_tambah.js') }}"></script>
    @endpush
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Pengguna - Tambah Baru
                    </h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Tambah Pengguna</h3>
                    </div>
                    <div class="card-body">
                        <form class="form" id="formAddPengguna">
                            <div class="form-group mb-3">
                                <label class="form-label">Nama <span class="err_notif_custom err_nama"></span> </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama">
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Email Username <span
                                        class="err_notif_custom err_email"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="username" id="username"
                                        placeholder="Email Username">
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Password <span
                                        class="err_notif_custom err_password"></span></label>
                                <div class="col-md-8">
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Level Pengguna <span
                                        class="err_notif_custom err_level"></span></label>
                                <div class="col-md-8">
                                    <select name="status" class="form-select" id="status">
                                        <option value="">Pilih level</option>
                                        @foreach ($level as $eachLevel)
                                            <option value="{{ $eachLevel->id }}">
                                                {{ $eachLevel->level_pengguna }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group mb-3 sub-bidang-field display-none">
                                <label class="form-label">Sub Bidang <span class="err_notif_custom err_bid"></span></label>
                                <div class="col-md-8">
                                    <select name="sub_bid" class="form-select" id="sub_bid">
                                    </select>
                                </div>
                            </div>
                            <div class="form-footer">
                                <a href="/pengguna" class="btn btn-default">Kembali</a>
                                <button type="submit" id="btnSubmit" class="btn btn-primary">Tambah Data</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
