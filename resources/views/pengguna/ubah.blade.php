@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/pengguna/pengguna_ubah.js') }}"></script>
    @endpush
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Pengguna - Ubah
                    </h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Ubah Pengguna</h3>
                    </div>
                    <div class="card-body">
                        <form class="form" id="formUbahPengguna" autocomplete="off">
                            <div class="form-group mb-3">
                                <label class="form-label">Nama <span class="err_notif_custom err_nama"></span> </label>
                                <div class="col-md-8">
                                    <input type="hidden" name="id_user" id="id_user" value="{{ $data->id }}">
                                    <input type="text" class="form-control" name="nama" value="{{ $data->nama }}"
                                        id="nama" placeholder="Nama">
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Email Username <span
                                        class="err_notif_custom err_email"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="username" autocomplete="off" value="{{ $data->username }}"
                                        id="username" placeholder="Email Username">
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Password <span
                                        class="err_notif_custom err_password"></span></label>
                                <div class="col-md-8">
                                    <input type="password" autocomplete="off" class="form-control" name="password"
                                        value="" id="password">

                                        <span style="font-size: 12px; color:blue">Isi password jika ingin merubah password</span>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Level Pengguna
                                    <span class="err_notif_custom err_level"></span>
                                </label>
                                <div class="col-md-8">
                                    <select name="status" class="form-select" id="status">
                                        <option value="">Pilih level</option>

                                        @foreach ($level as $eachLevel)
                                            <option @if ($eachLevel->id == $data->status) selected @endif value="{{ $eachLevel->id }}">
                                                {{ $eachLevel->level_pengguna }}
                                            </option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            @if ($data->status == 5)
                                <div class="form-group mb-3 sub-bidang-field">
                                    <label class="form-label">Sub Bidang <span
                                            class="err_notif_custom err_bid"></span></label>
                                    <div class="col-md-8">
                                        <select name="sub_bid" class="form-select" id="sub_bid">
                                            @foreach ($subBid as $subBidEach)
                                                <option @if ($subBidEach->id == $data->id_posisi) selected @endif value="{{$subBidEach->id}}">{{$subBidEach->sub_bid}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @else
                                    <div class="form-group mb-3 sub-bidang-field display-none">
                                        <label class="form-label">Sub Bidang <span
                                                class="err_notif_custom err_bid"></span></label>
                                        <div class="col-md-8">
                                            <select name="sub_bid" class="form-select" id="sub_bid">
                                            </select>
                                        </div>
                            @endif

                    </div>
                    <div class="form-footer">
                        <a href="/pengguna" class="btn btn-default">Kembali</a>
                        <button type="submit" id="btnSubmit" class="btn btn-primary">Simpan Perubahan</button>
                    </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
