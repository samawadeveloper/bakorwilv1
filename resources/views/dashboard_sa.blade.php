@extends('app')

@section('content')

    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/dashboard/dashboard_sa.js') }}"></script>
    @endpush

    <div class="container-xl">
        <!-- Page Header Title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <!-- Page pre-title -->
                    <div class="page-pretitle">
                        Overview
                    </div>
                    <h2 class="page-title">
                        Dashboard - {{ date('Y')}}
                    </h2>
                </div>
            </div>
        </div>
        <div class="row row-deck row-cards">
            {{-- Program --}}
            <div class="col-sm-3">
                <div class="card">
                  <div class="card-body bg-indigo p-2 text-center text-white">
                    <div class="text-end text-white">
                      <span class="text-white d-inline-flex align-items-center lh-1">
                        2021
                      </span>
                    </div>
                    <div class="h1 m-0">{{$cData['uMasterProgram']['cProg']}}</div>
                    <div class="text-muted text-white mb-3">Program</div>
                  </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                  <div class="card-body bg-azure p-2 text-center text-white">
                    <div class="text-end text-white">
                      <span class="text-white d-inline-flex align-items-center lh-1">
                        2021
                      </span>
                    </div>
                    <div class="h1 m-0">{{$cData['uMasterProgram']['cKeg']}}</div>
                    <div class="text-muted text-white mb-3">Kegiatan</div>
                  </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                  <div class="card-body p-2 bg-pink text-center text-white">
                    <div class="text-end text-white">
                      <span class="text-white d-inline-flex align-items-center lh-1">
                        2021
                      </span>
                    </div>
                    <div class="h1 m-0">{{$cData['uMasterProgram']['cSubKeg']}}</div>
                    <div class="text-muted text-white mb-3">Sub Kegiatan</div>
                  </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                  <div class="card-body bg-yellow p-2 text-center text-white">
                    <div class="text-end text-white">
                      <span class="text-white d-inline-flex align-items-center lh-1">
                        2021
                      </span>
                    </div>
                    <div class="h1 m-0">{{$cData['uMasterProgram']['cSubSubKeg']}}</div>
                    <div class="text-muted text-white mb-3">Sub Sub Kegiatan</div>
                  </div>
                </div>
            </div>

            {{-- User Activity --}}
            <div class="col-md-12 col-lg-4">
              <div class="card" style="height: calc(24rem + 10px)"">
                  <div class="card-header">
                     <h3 class="card-title">Aktifitas User</h3>
                  </div>
                  <div class="card-body card-body-scrollable card-body-scrollable-shadow">
                     <div class="divide-y-4">
                        @foreach ($cData['uListActivity'] as $cVal)
                        <div>
                           <div class="row">
                              <div class="col-auto">
                                 <span class="avatar" style="background-image: url(./static/avatars/002m.jpg)"></span>
                              </div>
                              <div class="col">
                                 <div class="text-truncate">
                                    <strong>{{$cVal->nama}}</strong>'s activity - <strong>"{{$cVal->nama_log}}"</strong>.
                                 </div>
                                 <div class="text-muted">{{$cVal->deskripsi}}</div>
                                 <div class="text-muted">{{$cVal->log_at}}</div>
                              </div>
                              <div class="col-auto align-self-center">
                                 <div class="badge bg-primary"></div>
                              </div>
                           </div>
                        </div>
                        @endforeach
                     </div>
                  </div>
              </div>
            </div>

            {{-- Menu Mastering --}}
            <div class="col-md-12 col-lg-8">
              <div class="card" style="height: calc(24rem + 10px)"">
                  <div class="card-header">
                     <h3 class="card-title">Pengguna</h3>
                  </div>
                  <div class="card-body card-body-scrollable card-body-scrollable-shadow">
                    <table class="table table-striped table-bordered zero-configuration dtable-pengguna">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                    </table>
                  </div>
              </div>
            </div>
        </div>


    </div>
@endsection
