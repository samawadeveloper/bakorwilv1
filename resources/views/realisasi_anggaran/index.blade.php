@extends('app')

@section('content')
    @push('custom-scripts')
        <script type="text/javascript" src="{{ URL::asset('assets/js/pages/realisasi/realisasi_anggaran.js') }}"></script>
    @endpush
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Realisasi Anggaran
                    </h2>
                </div>

            </div>

        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped table-bordered zero-configuration dtable-realisasi-anggaran" style="    font-size: 11px;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Program</th>
                                    <th>Kegiatan</th>
                                    <th>Sub Kegiatan</th>
                                    <th>Sub Kegiatan Bidang</th>
                                    <th>Pagu</th>
                                    <th>Realisasi</th>
                                </tr>
                            </thead>
                            <tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-blur fade" id="realisasi" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form class="form frm-realisasi" id="formRealisasi">
                    <div class="modal-header">
                        <h5 class="modal-title">Realisasi Anggaran</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div>
                            <label class="form-label">Pagu</label>
                            <p id="pagu_txt"></p>
                            <input type="hidden" id="id_kegiatan_sub_bidang" class="form-control" />
                        </div>
                        <div>
                            <label class="form-label">Realisasi <span class="err_notif_custom err_realisasi"></span></label>
                            <input type="text" class="form-control" id="realisasi_txt">
                        </div>
                        <div style="margin-top:10px;">
                            <label class="form-label">Keterangan</label>
                            <textarea class="form-control" id="keterangan"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="btnSubmitRealisasi" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-blur fade" id="realisasi-update" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form class="form frm-realisasi" id="formRealisasiUpdate">
                    <div class="modal-header">
                        <h5 class="modal-title">Realisasi Anggaran</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div>
                            <label class="form-label">Pagu</label>
                            <p id="pagu_txt_update"></p>
                            <input type="hidden" id="id_realisasi_anggaran" class="form-control" />
                        </div>
                        <div>
                            <label class="form-label">Realisasi <span class="err_notif_custom err_realisasi_update"></span></label>
                            <input type="text" class="form-control" id="realisasi_update_txt">
                        </div>
                        <div style="margin-top:10px;">
                            <label class="form-label">Keterangan</label>
                            <textarea class="form-control" id="keterangna_update"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="btnSubmitRealisasiUpdate" class="btn btn-primary">Simpan Perubahan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
