console.log("load source js file");

$("#formAddSubBidang").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var sub_bid = $("#sub_bid").val();

    if (sub_bid === "") {
        $("#sub_bid").focus();
        $(".err_sub_bidang").html("Isi Sub Bidang!");
    } else {
        $("#btnSubmit").text("Process ...");
        $("#btnSubmit").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("sub_bid", sub_bid);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: 'proses-tambah-sub-bidang',
            type: 'POST',
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function(data) {
                $("#btnSubmit").removeAttr('disabled');
                $("#btnSubmit").text('Submit');

                location.href = "/sub-bidang";

            },
            error: function(xhr, status, error) {
                $("#btnSubmit").removeAttr('disabled');
                $("#btnSubmit").text('Submit');
                alert(xhr.responseText);
            }
        });

    }
});