console.log("loaded Pengguna Source Js");

loadData();

function loadData() {

    $.ajax({
        processing: true,
        serverSide: true,
        type: "GET",
        url: "/sub-bidang",
        dataType: "json",
        cache: false,
        success: function(obj) {
            $(".loading").css("display", "none");
            $(".dtable-sub-bidang").DataTable({
                data: obj.data,
                columns: [{
                        data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                    },
                    {
                        render: function(data, type, row, meta) {
                            return row.sub_bid;
                        },
                    },
                    { data: "action", name: "action", orderable: false },
                ],
                columnDefs: [{
                    targets: "no-sort",
                    orderable: false,
                }, ],
                oLanguage: {
                    sLengthMenu: "Menampilkan _MENU_ data",
                    sSearch: "Cari",
                    sInfo: "Menampilkan _START_ - _END_ dari _TOTAL_ data",
                    oPaginate: {
                        sFirst: "Pertama", // This is the link to the first page
                        sPrevious: "Sebelumnya", // This is the link to the previous page
                        sNext: "Selanjutnya", // This is the link to the next page
                        sLast: "Terakhir", // This is the link to the last page
                    },
                },
                responsive: true,
                bDestroy: true,
            });
        },
        error: function(obj, textstatus) {
            alert(obj.msg);
        },
    });

}