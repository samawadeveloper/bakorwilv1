console.log("loaded Pengguna Source Js");

loadData();

function loadData() {

    $.ajax({
        processing: true,
        serverSide: true,
        type: "GET",
        url: "/pengguna",
        dataType: "json",
        cache: false,
        success: function(obj) {
            $(".loading").css("display", "none");
            $(".dtable-pengguna").DataTable({
                data: obj.data,
                columns: [{
                        data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                    },
                    {
                        render: function(data, type, row, meta) {
                            return row.nama;
                        },
                    },
                    {
                        render: function(data, type, row, meta) {
                            if (row.status === 1) {
                                return "Superadmin";
                            } else if (row.status === 2) {
                                return "Admin";
                            } else if (row.status === 3) {
                                return "Kepala Bakorwil";
                            } else if (row.status === 6) {
                                return "Keuangan";
                            } else if (row.status === 4) {
                                return "Kepala Bidang";
                            } else if (row.status === 5) {
                                return "Sub Bidang " + row.sub_bid;
                            } else {
                                return "-";
                            }
                            // return row.status + ' ' + row.sub_bid;
                        },
                    },
                    {
                        render: function(data, type, row, meta) {
                            if (row.aktif === 1) {
                                return "Aktif";
                            } else {
                                return "Non Aktif";
                            }
                        },
                    },
                    { data: "action", name: "action", orderable: false },
                ],
                columnDefs: [{
                    targets: "no-sort",
                    orderable: false,
                }, ],
                oLanguage: {
                    sLengthMenu: "Menampilkan _MENU_ data",
                    sSearch: "Cari",
                    sInfo: "Menampilkan _START_ - _END_ dari _TOTAL_ data",
                    oPaginate: {
                        sFirst: "Pertama", // This is the link to the first page
                        sPrevious: "Sebelumnya", // This is the link to the previous page
                        sNext: "Selanjutnya", // This is the link to the next page
                        sLast: "Terakhir", // This is the link to the last page
                    },
                },
                responsive: true,
                bDestroy: true,
            });
        },
        error: function(obj, textstatus) {
            alert(obj.msg);
        },
    });

}

function showModalPengguna(id, nama, status) {
    $("#nama_pengguna_modal").html(nama);
    $("#id_user").val(id);
    $("#nama").val(nama);
    $("#catTxt").html(status);
}

$("#btnSubmit").click(function(e) {
    $(".loading").css("display", "block");
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/proses-aktifasi-pengguna/' + $("#id_user").val(),
        type: 'GET',
        contentType: false,
        processData: false,

        success: function(data) {
            $("#btnSubmit").removeAttr('disabled');
            $("#btnSubmit").text('Submit');

            loadData();

        },
        error: function(xhr, status, error) {
            $("#btnSubmit").removeAttr('disabled');
            $("#btnSubmit").text('Submit');
            alert(xhr.responseText);
        }
    });
});