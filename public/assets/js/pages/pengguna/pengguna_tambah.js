console.log("load source js file");

$("#status").change(function() {
    var status = $(this).val();
    if (status === "5") {
        $(".sub-bidang-field").removeClass("display-none");
        $.ajax({
            processing: true,
            serverSide: true,
            type: "GET",
            url: "/get-sub-bidang",
            dataType: "json",
            cache: false,
            success: function(obj) {
                $("#sub_bid").html(obj.data);
            },
            error: function(xhr, status, error) {
                alert(xhr.responseText);
            },
        });
    } else {
        $(".sub-bidang-field").addClass("display-none");
        $("#sub_bid").html("");
    }
});

$("#formAddPengguna").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var nama = $("#nama").val();
    var username = $("#username").val();
    var password = $("#password").val();
    var status = $("#status").val();

    if (nama === "") {
        $("#nama").focus();
        $(".err_nama").html("Isi nama pengguna!");
    } else if (username === "") {
        $("#username").focus();
        $(".err_email").html("Isi username!");
    } else if (password === "") {
        $("#password").focus();
        $(".err_password").html("Password username!");
    } else if (status === "") {
        $("#password").focus();
        $(".err_level").html("Pilih Status!");
    } else {
        $("#btnSubmit").text("Process ...");
        $("#btnSubmit").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("nama", nama);
        form_data.append("username", username);
        form_data.append("password", password);
        form_data.append("status", status);
        if (status === "5") {
            form_data.append("sub_bid", $("#sub_bid").val());
        }

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: 'proses-tambah-pengguna',
            type: 'POST',
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function(data) {
                $("#btnSubmit").removeAttr('disabled');
                $("#btnSubmit").text('Submit');

                location.href = "/pengguna";

            },
            error: function(xhr, status, error) {
                $("#btnSubmit").removeAttr('disabled');
                $("#btnSubmit").text('Submit');
                alert(xhr.responseText);
            }
        });

    }
});