console.log("loaded js!");

$("#formAddProgram").submit(function(e) {
    $(".loading").css("display", "block");
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var kode = $("#kode").val();
    var program = $("#program").val();

    if (kode === "") {
        $("#kode").focus();
        $(".err_kode").html("Isi Kode!");
        $(".loading").css("display", "none");
    } else if (program === "") {
        $("#program").focus();
        $(".err_program").html("Isi Program!");
        $(".loading").css("display", "none");
    } else {
        $("#btnSubmit").text("Process ...");
        $("#btnSubmit").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("kode", kode);
        form_data.append("program", program);

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "proses-tambah-program",
            type: "POST",
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function(data) {
                $("#btnSubmit").removeAttr("disabled");
                $("#btnSubmit").text("Submit");
                $(".loading").css("display", "none");
                $("#program-baru").modal("hide");
                $("#kode").val("");
                loadData();
            },
            error: function(xhr, status, error) {
                $("#btnSubmit").removeAttr("disabled");
                $("#btnSubmit").text("Submit");
                alert(xhr.responseText);
            },
        });
    }
});

$("#formAddKegiatan").submit(function(e) {
    $(".loading").css("display", "block");
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var kode = $("#kode_kegiatan").val();
    var kegiatan = $("#kegiatan").val();
    var id_program = $("#id_program").val();

    if (kode === "") {
        $("#kode_kegiatan").focus();
        $(".err_kode_kegiatan").html("Isi Kode!");
        $(".loading").css("display", "none");
    } else if (kegiatan === "") {
        $("#kegiatan").focus();
        $(".err_kegiatan").html("Isi Kegiatan!");
        $(".loading").css("display", "none");
    } else {
        $("#btnSubmitKegiatan").text("Process ...");
        $("#btnSubmitKegiatan").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("kode", kode);
        form_data.append("kegiatan", kegiatan);
        form_data.append("id_program", id_program);

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "proses-tambah-kegiatan",
            type: "POST",
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function(data) {
                $("#btnSubmitKegiatan").removeAttr("disabled");
                $("#btnSubmitKegiatan").text("Submit");
                $(".loading").css("display", "none");
                $("#kegiatan-baru").modal("hide");
                $("#kode_kegiatan").val("");
                $("#kegiatan").val("");
                $("#id_program").val("");
                loadData();
            },
            error: function(xhr, status, error) {
                $("#btnSubmitKegiatan").removeAttr("disabled");
                $("#btnSubmitKegiatan").text("Submit");
                alert(xhr.responseText);
            },
        });
    }
});

$("#formAddSubKegiatan").submit(function(e) {
    $(".loading").css("display", "block");
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var kode = $("#kode_sub_kegiatan").val();
    var sub_kegiatan = $("#sub_kegiatan").val();
    var id_kegiatan = $("#id_kegiatan").val();

    if (kode === "") {
        $("#kode_sub_kegiatan").focus();
        $(".err_kode_subkegiatan").html("Isi Kode!");
        $(".loading").css("display", "none");
    } else if (sub_kegiatan === "") {
        $("#sub_kegiatan").focus();
        $(".err_subkegiatan").html("Isi Kegiatan!");
        $(".loading").css("display", "none");
    } else {
        $("#btnSubmitSubKegiatan").text("Process ...");
        $("#btnSubmitSubKegiatan").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("kode", kode);
        form_data.append("sub_kegiatan", sub_kegiatan);
        form_data.append("id_kegiatan", id_kegiatan);

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "proses-tambah-sub-kegiatan",
            type: "POST",
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function(data) {
                $("#btnSubmitSubKegiatan").removeAttr("disabled");
                $("#btnSubmitSubKegiatan").text("Submit");
                $(".loading").css("display", "none");
                $("#subkegiatan-baru").modal("hide");
                $("#kode_sub_kegiatan").val("");
                $("#sub_kegiatan").val("");
                $("#id_kegiatan").val("");
                loadData();
            },
            error: function(xhr, status, error) {
                $("#btnSubmitSubKegiatan").removeAttr("disabled");
                $("#btnSubmitSubKegiatan").text("Submit");
                alert(xhr.responseText);
            },
        });
    }
});
$("#formAddSubSubKegiatan").submit(function(e) {
    $(".loading").css("display", "block");
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var kode = $("#kode_sub_sub_kegiatan").val();
    var sub_sub_kegiatan = $("#sub_sub_kegiatan").val();
    var id_sub_kegiatan = $("#id_sub_kegiatan").val();
    var sub_bidang = $("#sub_bidang").val();

    if (kode === "") {
        $("#kode_sub_sub_kegiatan").focus();
        $(".err_kode_subsubkegiatan").html("Isi Kode!");
        $(".loading").css("display", "none");
    } else if (sub_sub_kegiatan === "") {
        $("#sub_sub_kegiatan").focus();
        $(".err_subsubkegiatan").html("Isi Kegiatan!");
        $(".loading").css("display", "none");
    } else if (sub_bidang === "") {
        $("#sub_bidang").focus();
        $(".err_subbidang").html("Pilih Bidang!");
        $(".loading").css("display", "none");
    } else {
        $("#btnSubmitSubSubKegiatan").text("Process ...");
        $("#btnSubmitSubSubKegiatan").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("kode", kode);
        form_data.append("sub_sub_kegiatan", sub_sub_kegiatan);
        form_data.append("id_sub_kegiatan", id_sub_kegiatan);
        form_data.append("id_sub_bidang", sub_bidang);

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "proses-tambah-sub-bidang-kegiatan",
            type: "POST",
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function(data) {
                $("#btnSubmitSubSubKegiatan").removeAttr("disabled");
                $("#btnSubmitSubKegiatan").text("Submit");
                $(".loading").css("display", "none");
                $("#subbidang-baru").modal("hide");
                $("#kode_sub_sub_kegiatan").val("");
                $("#sub_sub_kegiatan").val("");
                $("#id_sub_kegiatan").val("");
                $("#sub_bidang").val("");
                loadData();
            },
            error: function(xhr, status, error) {
                $("#btnSubmitSubKegiatan").removeAttr("disabled");
                $("#btnSubmitSubKegiatan").text("Submit");
                alert(xhr.responseText);
            },
        });
    }
});
$("#formAddIndikatorProgram").submit(function(e) {
    $(".loading").css("display", "block");
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var id_program_indikator = $("#id_program_indikator").val();
    var indikator_program = $("#indikator_program").val();
    var satuan_program = $("#satuan_program").val();

    if (indikator_program === "") {
        $("#indikator_program").focus();
        $(".err_indikator_program").html("Isi Indikator Program!");
        $(".loading").css("display", "none");
    } else if (satuan_program === "") {
        $("#satuan_program").focus();
        $(".err_satuan").html("Isi Satuan!");
        $(".loading").css("display", "none");
    } else {
        $("#btnSubmitIndikatorProgram").text("Process ...");
        $("#btnSubmitIndikatorProgram").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("id_program_indikator", id_program_indikator);
        form_data.append("indikator_program", indikator_program);
        form_data.append("satuan_program", satuan_program);

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "proses-tambah-indikator-program",
            type: "POST",
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function(data) {
                $("#btnSubmitIndikatorProgram").removeAttr("disabled");
                $("#btnSubmitIndikatorProgram").text("Submit");
                $(".loading").css("display", "none");
                $("#indikator-program").modal("hide");
                $("#id_program_indikator").val("");
                $("#indikator_program").val("");
                $("#satuan_program").val("");
                loadData();
            },
            error: function(xhr, status, error) {
                $("#btnSubmitIndikatorProgram").removeAttr("disabled");
                $("#btnSubmitIndikatorProgram").text("Submit");
                alert(xhr.responseText);
            },
        });
    }
});
$("#formAddIndikatorKegiatan").submit(function(e) {
    $(".loading").css("display", "block");
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var id_kegiatan_indikator = $("#id_kegiatan_indikator").val();
    var indikator_kegiatan = $("#indikator_kegiatan").val();
    var satuan_kegiatan = $("#satuan_kegiatan").val();

    if (indikator_kegiatan === "") {
        $("#indikator_kegiatan").focus();
        $(".err_indikator_kegiatan").html("Isi Indikator Kegiatan!");
        $(".loading").css("display", "none");
    } else if (satuan_kegiatan === "") {
        $("#satuan_kegiatan").focus();
        $(".err_satuan_kegiatan").html("Isi Satuan Kegiatan!");
        $(".loading").css("display", "none");
    } else {
        $("#btnSubmitIndikatorKegiatan").text("Process ...");
        $("#btnSubmitIndikatorKegiatan").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("id_kegiatan_indikator", id_kegiatan_indikator);
        form_data.append("indikator_kegiatan", indikator_kegiatan);
        form_data.append("satuan_kegiatan", satuan_kegiatan);

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "proses-tambah-indikator-kegiatan",
            type: "POST",
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function(data) {
                $("#btnSubmitIndikatorKegiatan").removeAttr("disabled");
                $("#btnSubmitIndikatorKegiatan").text("Submit");
                $(".loading").css("display", "none");
                $("#indikator-kegiatan").modal("hide");
                $("#id_kegiatan_indikator").val("");
                $("#indikator_kegiatan").val("");
                $("#satuan_kegiatan").val("");
                loadData();
            },
            error: function(xhr, status, error) {
                $("#btnSubmitIndikatorKegiatan").removeAttr("disabled");
                $("#btnSubmitIndikatorKegiatan").text("Submit");
                alert(xhr.responseText);
            },
        });
    }
});

$("#formAddIndikatorSubKegiatan").submit(function(e) {
    $(".loading").css("display", "block");
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var id_sub_kegiatan_indikator = $("#id_sub_kegiatan_indikator").val();
    var indikator_sub_kegiatan = $("#indikator_sub_kegiatan").val();
    var satuan_sub_kegiatan = $("#satuan_sub_kegiatan").val();

    if (indikator_sub_kegiatan === "") {
        $("#indikator_sub_kegiatan").focus();
        $(".err_indikator_sub_kegiatan").html("Isi Indikator!");
        $(".loading").css("display", "none");
    } else if (satuan_sub_kegiatan === "") {
        $("#satuan_sub_kegiatan").focus();
        $(".err_satuan_sub_kegiatan").html("Isi Satuan!");
        $(".loading").css("display", "none");
    } else {
        $("#btnSubmitIndikatorSubKegiatan").text("Process ...");
        $("#btnSubmitIndikatorSubKegiatan").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append(
            "id_sub_kegiatan_indikator",
            id_sub_kegiatan_indikator
        );
        form_data.append("indikator_sub_kegiatan", indikator_sub_kegiatan);
        form_data.append("satuan_sub_kegiatan", satuan_sub_kegiatan);

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "proses-tambah-indikator-subkegiatan",
            type: "POST",
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function(data) {
                $("#btnSubmitIndikatorSubKegiatan").removeAttr("disabled");
                $("#btnSubmitIndikatorSubKegiatan").text("Submit");
                $(".loading").css("display", "none");
                $("#indikator-subkegiatan").modal("hide");
                $("#id_sub_kegiatan_indikator").val("");
                $("#indikator_sub_kegiatan").val("");
                $("#satuan_sub_kegiatan").val("");
                loadData();
            },
            error: function(xhr, status, error) {
                $("#btnSubmitIndikatorSubKegiatan").removeAttr("disabled");
                $("#btnSubmitIndikatorSubKegiatan").text("Submit");
                alert(xhr.responseText);
            },
        });
    }
});
$("#formAddIndikatorSubBidang").submit(function(e) {
    $(".loading").css("display", "block");
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var id_sub_bidang_indikator = $("#id_sub_bidang_indikator").val();
    var indikator_sub_bidang = $("#indikator_sub_bidang").val();
    var satuan_sub_bidang = $("#satuan_sub_bidang").val();

    if (indikator_sub_bidang === "") {
        $("#indikator_sub_bidang").focus();
        $(".err_indikator_sub_bidang").html("Isi Indikator!");
        $(".loading").css("display", "none");
    } else if (satuan_sub_bidang === "") {
        $("#satuan_sub_bidang").focus();
        $(".err_satuan_sub_bidang").html("Isi Satuan!");
        $(".loading").css("display", "none");
    } else {
        $("#btnSubmitIndikatorSubBidang").text("Process ...");
        $("#btnSubmitIndikatorSubBidang").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("id_sub_bidang_indikator", id_sub_bidang_indikator);
        form_data.append("indikator_sub_bidang", indikator_sub_bidang);
        form_data.append("satuan_sub_bidang", satuan_sub_bidang);

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "proses-tambah-indikator-subbid",
            type: "POST",
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function(data) {
                $("#btnSubmitIndikatorSubBidang").removeAttr("disabled");
                $("#btnSubmitIndikatorSubBidang").text("Submit");
                $(".loading").css("display", "none");
                $("#indikator-subbidang").modal("hide");
                $("#id_sub_bidang_indikator").val("");
                $("#indikator_sub_bidang").val("");
                $("#satuan_sub_bidang").val("");
                loadData();
            },
            error: function(xhr, status, error) {
                $("#btnSubmitIndikatorSubBidang").removeAttr("disabled");
                $("#btnSubmitIndikatorSubBidang").text("Submit");
                alert(xhr.responseText);
            },
        });
    }
});
$("#formTarget").submit(function(e) {
    $(".loading").css("display", "block");
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var id_indikator = $("#id_indikator").val();
    var tahun = $("#tahun").val();
    var target_modal = $("#target_modal").val();

    if (tahun === "") {
        $("#tahun").focus();
        $(".err_tahun").html("Isi Tahun!");
        $(".loading").css("display", "none");
    } else if (target_modal === "") {
        $("#target_modal").focus();
        $(".err_target").html("Isi Target!");
        $(".loading").css("display", "none");
    } else {
        $("#btnSubmitTarget").text("Process ...");
        $("#btnSubmitTarget").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("id_indikator", id_indikator);
        form_data.append("tahun", tahun);
        form_data.append("target", target_modal);

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: $(this).attr("action"),
            type: "POST",
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function(data) {
                $("#btnSubmitTarget").removeAttr("disabled");
                $("#btnSubmitTarget").text("Submit");
                $(".loading").css("display", "none");
                $("#target").modal("hide");
                $("#id_indikator").val("");
                $("#tahun").val("");
                $("#target_modal").val("");
                loadData();
            },
            error: function(xhr, status, error) {
                $("#btnSubmitTarget").removeAttr("disabled");
                $("#btnSubmitTarget").text("Submit");
                alert(xhr.responseText);
            },
        });
    }
});
$("#formPagu").submit(function(e) {
    $(".loading").css("display", "block");
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var id_sub_bidang = $("#id_sub_bidang").val();
    var pagu = $("#pagu_txt").val();

    if (pagu === "") {
        $("#pagu_txt").focus();
        $(".err_pagu").html("Isi Pagu!");
        $(".loading").css("display", "none");
    } else {
        $("#btnSubmitPagu").text("Process ...");
        $("#btnSubmitPagu").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("id_sub_bidang", id_sub_bidang);
        form_data.append("pagu", pagu);

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "proses-set-pagu",
            type: "POST",
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function(data) {
                $("#btnSubmitPagu").removeAttr("disabled");
                $("#btnSubmitPagu").text("Submit");
                $(".loading").css("display", "none");
                $("#pagu").modal("hide");
                $("#id_sub_bidang").val("");
                $("#pagu_txt").val("");
                loadData();
            },
            error: function(xhr, status, error) {
                $("#btnSubmitPagu").removeAttr("disabled");
                $("#btnSubmitPagu").text("Submit");
                alert(xhr.responseText);
            },
        });
    }
});

// loadData();

function loadData() {
    $(".table-program tbody").html("");
    $.ajax({
        processing: true,
        serverSide: true,
        type: "GET",
        url: "/ajax-program",
        dataType: "json",
        cache: false,
        success: function(obj) {
            $.each(obj.data, function(i, item) {
                if (item.indikator === null) {
                    var indikator = "";
                    var satuan = "";
                } else {
                    var indikator = item.indikator;

                    var satuan = item.satuan;
                }
                $("<tr id='program" + item.id + "'>")
                    .append(
                        $(
                            "<td style='vertical-align:top;background:#e5e5e5;font-weight:bold;font-size: 11px;'>"
                        ).html(item.kode),
                        $(
                            "<td style='vertical-align:top;background:#e5e5e5;font-weight:bold;font-size: 11px;'>"
                        ).html(
                            item.program +
                            '<br><a style="color:#9b9b9b; background:#235789;font-size:10px;padding:3px;border-radius:5px;color:#ffffff" href="javascript:void(0)" onclick="setIdProgram(' +
                            item.id +
                            ')"  data-bs-toggle="modal" data-bs-target="#kegiatan-baru"><svg xmlns="http://www.w3.org/2000/svg" style="width:13px;height:13px;" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg> Kegiatan</a></p> '
                        ),
                        $(
                            "<td style='vertical-align:top;background:#e5e5e5;font-weight:500;font-size: 11px;'>"
                        ).html(
                            '<span style="font-size:9px;">' +
                            indikator +
                            '</span> &nbsp; <div id="isIndikator"></div>'
                        ),
                        $(
                            "<td style='vertical-align: top;padding:0;background:#e5e5e5;font-weight:500;font-size: 11px;'>"
                        ).html(
                            '<table class="table" style="padding:0;margin-bottom:5px !important" id="showTarget' +
                            item.id +
                            '"></table><div id="subTargetProgra"></div>'
                        ),
                        $(
                            "<td style='background:#e5e5e5;font-weight:500;font-size: 11px;text-align:center'>"
                        ).text(satuan),
                        $(
                            "<td style='background:#e5e5e5;font-weight:500;font-size: 11px;text-align:center'>"
                        ).text("")
                    )
                    .appendTo(".table-program tbody");
                if (item.indikator === null) {
                    $("#isIndikator").html(
                        '<a href="javascript:void(0)" onclick="setIdProgramIndikator(' +
                        item.id +
                        ')" data-bs-toggle="modal" data-bs-target="#indikator-program"><svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><rect x="4" y="4" width="16" height="16" rx="2" /><line x1="9" y1="12" x2="15" y2="12" /><line x1="12" y1="9" x2="12" y2="15" /></svg></a>'
                    );
                }
                var thn = item.tahun;
                var target = item.target;
                var result = thn.split(",");
                var resultTarget = target.split(",");
                if (thn === "") {
                    var divHtml = "<tr>";
                    for (var s = 0; s < result.length; s++) {
                        divHtml +=
                            "<td style='font-weight:500;font-size:10px;padding:5px;text-align:center'>" +
                            result[s] +
                            "</td>";
                    }

                    divHtml +=
                        '<td rowspan="2" style="width:20px;"><div align="center"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#target" onclick="setPropTarget(' +
                        item.id_indikator_program +
                        ',\'proses-tambah-target-program\')" ><svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><rect x="4" y="4" width="16" height="16" rx="2" /><line x1="9" y1="12" x2="15" y2="12" /><line x1="12" y1="9" x2="12" y2="15" /></svg></a></div></td></tr><tr>';

                    for (var tr = 0; tr < resultTarget.length; tr++) {
                        divHtml +=
                            "<td style='font-weight:500;font-size:10px;padding:0;text-align:center'>" +
                            resultTarget[tr] +
                            "</td>";
                    }
                    divHtml += "</tr>";
                } else {
                    for (var s = 0; s < result.length; s++) {
                        divHtml +=
                            "<tr><td style='font-weight:500;font-size:10px;padding:5px;text-align:center'>" +
                            result[s] +
                            "</td></tr>";
                    }

                    for (var tr = 0; tr < resultTarget.length; tr++) {
                        divHtml +=
                            "<tr><td style='font-weight:500;font-size:10px;padding:0;text-align:center'>" +
                            resultTarget[tr] +
                            "</td></tr>";
                    }
                }

                $("#showTarget" + item.id).append(divHtml);
            });

            $.each(obj.kegiatan, function(i, item) {
                if (item.indikator === null) {
                    var indikator = "";
                    var satuan = "";
                } else {
                    var indikator = item.indikator;

                    var satuan = item.satuan;
                }
                $("<tr id='kegiatan" + item.id_kegiatan + "'>")
                    .append(
                        $(
                            "<td style='background:#f2f2f2;font-weight:500;'>"
                        ).html(item.kode_program + "." + item.kode),
                        $(
                            "<td style='background:#f2f2f2;font-weight:500;'>"
                        ).html(
                            item.kegiatan +
                            "<a style='color:#9b9b9b; background:gray;font-size:10px; margin-left:10px; padding:3px;border-radius:5px;color:#ffffff' href='javascript:void(0)' onclick='setIdKegiatan(" +
                            item.id_kegiatan +
                            ")'  data-bs-toggle='modal' data-bs-target='#subkegiatan-baru'><svg xmlns='http://www.w3.org/2000/svg' style='width:13px;height:13px;' class='icon' width='24' height='24' viewBox='0 0 24 24' stroke-width='2' stroke=currentColor fill=none stroke-linecap=round stroke-linejoin='round'><path stroke='none' d='M0 0h24v24H0z' fill='none'/><line x1='12' y1='5' x2='12' y2='19'/><line x1='5' y1='12' x2='19' y2='12' /></svg>Sub Kegiatan</a>"
                        ),
                        $(
                            "<td style='background:#f2f2f2;font-weight:500;'>"
                        ).html(
                            indikator + '&nbsp; <div id="isIndikator"></div>'
                        ),
                        $(
                            "<td style='background:#f2f2f2;font-weight:500;padding:0'>"
                        ).html(
                            '<table class="table" style="padding:0;margin-bottom:5px !important" id="showTargetKeg' +
                            item.id_kegiatan +
                            '"></table><div align="center" id="subTargetKeg"></div>'
                        ),
                        $(
                            "<td style='background:#f2f2f2;font-weight:500;font-size: 11px;text-align:center'>"
                        ).text(satuan),
                        $(
                            "<td style='background:#f2f2f2;font-weight:500;'>"
                        ).text("")
                    )
                    .insertAfter("#program" + item.id_program);
                if (item.indikator === null) {
                    $("#isIndikator").html(
                        '<a href="javascript:void(0)" onclick="setIdKegiatanIndikator(' +
                        item.id_kegiatan +
                        ')" data-bs-toggle="modal" data-bs-target="#indikator-kegiatan"><svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><rect x="4" y="4" width="16" height="16" rx="2" /><line x1="9" y1="12" x2="15" y2="12" /><line x1="12" y1="9" x2="12" y2="15" /></svg></a>'
                    );
                }
                // <div align="center "></div>

                var thn = item.tahun;
                var target = item.target;
                var result = thn.split(",");
                var resultTarget = target.split(",");

                if (thn === "") {
                    var divHtml = "<tr>";
                    for (var s = 0; s < result.length; s++) {
                        divHtml +=
                            "<td style='font-weight:500;font-size:10px;padding:5px;text-align:center'>" +
                            result[s] +
                            "</td>";
                    }
                    if (item.id_indikator_kegiatan !== null) {
                        divHtml +=
                            '<td rowspan="2" style="width:20px;"><div align="center"><a href="javascript: void(0)" data-bs-toggle="modal" data-bs-target="#target" onclick="setPropTarget(' +
                            item.id_indikator_kegiatan +
                            ', \'proses-tambah-target-kegiatan\')" ><svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><rect x="4" y="4" width="16" height="16" rx="2" /><line x1="9" y1="12" x2="15" y2="12" /><line x1="12" y1="9" x2="12" y2="15" /></svg></a></div></td></tr><tr>';
                    }
                    for (var tr = 0; tr < resultTarget.length; tr++) {
                        divHtml +=
                            "<td style='font-weight:500;font-size:10px;padding:0;text-align:center'>" +
                            resultTarget[tr] +
                            "</td>";
                    }
                    divHtml += "</tr>";
                } else {
                    for (var s = 0; s < result.length; s++) {
                        divHtml +=
                            "<tr><td style='font-weight:500;font-size:10px;padding:5px;text-align:center'>" +
                            result[s] +
                            "</td></tr>";
                    }
                    for (var tr = 0; tr < resultTarget.length; tr++) {
                        divHtml +=
                            "<tr><td style='font-weight:500;font-size:10px;padding:0;text-align:center'>" +
                            resultTarget[tr] +
                            "</td></tr>";
                    }
                }

                $("#showTargetKeg" + item.id_kegiatan).append(divHtml);
            });

            $.each(obj.sub_kegiatan, function(i, item) {
                if (item.indikator === null) {
                    var indikator = "";
                    var satuan = "";
                } else {
                    var indikator = item.indikator;

                    var satuan = item.satuan;
                }
                $("<tr id='subkegiatan" + item.id_sub_kegiatan + "'>")
                    .append(
                        $(
                            "<td style='background:#f9f9f9;font-weight:500;'>"
                        ).html(
                            item.kode_program +
                            "." +
                            item.kode_kegiatan +
                            "." +
                            item.kode
                        ),
                        $(
                            "<td style='background:#f9f9f9;font-weight:500;'>"
                        ).html(
                            item.sub_kegiatan +
                            "<a style='color:brown; background:#ffd59e;font-size:10px; margin-left:10px; padding:3px;border-radius:5px;color:#ffffff' href='javascript:void(0)' onclick='setIdSubKegiatan(" +
                            item.id_sub_kegiatan +
                            ")'  data-bs-toggle='modal' data-bs-target='#subbidang-baru'><svg xmlns='http://www.w3.org/2000/svg' style='width:13px;height:13px;' class='icon' width='24' height='24' viewBox='0 0 24 24' stroke-width='2' stroke=currentColor fill=none stroke-linecap=round stroke-linejoin='round'><path stroke='none' d='M0 0h24v24H0z' fill='none'/><line x1='12' y1='5' x2='12' y2='19'/><line x1='5' y1='12' x2='19' y2='12' /></svg>Sub Bidang</a>"
                        ),
                        $(
                            "<td style='background:#f9f9f9;font-weight:500;'>"
                        ).html(
                            indikator + '&nbsp; <div id="isIndikator"></iv>'
                        ),
                        $(
                            "<td style='background:#f9f9f9;font-weight:500;padding:0'>"
                        ).html(
                            '<table class="table" style="padding:0;margin-bottom:5px !important" id="showTargetSUbKeg' +
                            item.id_sub_kegiatan +
                            '"></table><div align="center" id="subTargetSubKeg"></div>'
                        ),
                        $(
                            "<td style='background:#f9f9f9;font-weight:500;font-size: 11px;text-align:center'>"
                        ).text(satuan),
                        $(
                            "<td style='background:#f9f9f9;font-weight:500;'>"
                        ).text("")
                    )
                    .insertAfter("#kegiatan" + item.id_kegiatan);
                if (item.indikator === null) {
                    $("#isIndikator").html(
                        '<a href="javascript:void(0)" onclick="setIdSubKegiatanIndikator(' +
                        item.id_sub_kegiatan +
                        ')" data-bs-toggle="modal" data-bs-target="#indikator-subkegiatan"><svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><rect x="4" y="4" width="16" height="16" rx="2" /><line x1="9" y1="12" x2="15" y2="12" /><line x1="12" y1="9" x2="12" y2="15" /></svg></a>'
                    );
                }

                var thn = item.tahun;
                var target = item.target;
                var result = thn.split(",");
                var resultTarget = target.split(",");

                if (thn === "") {
                    var divHtml = "<tr>";
                    for (var s = 0; s < result.length; s++) {
                        divHtml +=
                            "<td style='font-weight:500;font-size:10px;padding:5px;text-align:center'>" +
                            result[s] +
                            "</td>";
                    }

                    if (item.id_indikator_sub_kegiatan !== null) {
                        divHtml +=
                            '<td rowspan="2" style="width:20px;"><div align="center"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#target" onclick="setPropTarget(' +
                            item.id_indikator_sub_kegiatan +
                            ',\'proses-tambah-target-sub-kegiatan\')" ><svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><rect x="4" y="4" width="16" height="16" rx="2" /><line x1="9" y1="12" x2="15" y2="12" /><line x1="12" y1="9" x2="12" y2="15" /></svg></a></div></td></tr><tr>';
                    }
                    for (var tr = 0; tr < resultTarget.length; tr++) {
                        divHtml +=
                            "<td style='font-weight:500;font-size:10px;padding:0;text-align:center'>" +
                            resultTarget[tr] +
                            "</td>";
                    }
                    divHtml += "</tr>";
                } else {
                    for (var s = 0; s < result.length; s++) {
                        divHtml +=
                            "<tr><td style='font-weight:500;font-size:10px;padding:5px;text-align:center'>" +
                            result[s] +
                            "</td></tr>";
                    }

                    for (var tr = 0; tr < resultTarget.length; tr++) {
                        divHtml +=
                            "<tr><td style='font-weight:500;font-size:10px;padding:0;text-align:center'>" +
                            resultTarget[tr] +
                            "</td></tr>";
                    }
                }

                $("#showTargetSUbKeg" + item.id_sub_kegiatan).append(divHtml);
            });

            $.each(obj.sub_bidang, function(i, item) {
                if (item.indikator === null) {
                    var indikator = "";
                    var satuan = "";
                } else {
                    var indikator = item.indikator;

                    var satuan = item.satuan;
                }
                $("<tr>")
                    .append(
                        $(
                            "<td style='background:#FFFFFF;font-weight:500;'>"
                        ).html(
                            item.kode_program +
                            "." +
                            item.kode_kegiatan +
                            "." +
                            item.kode_sub_kegiatan +
                            "." +
                            item.kode
                        ),
                        $(
                            "<td style='background:#FFFFFF;font-weight:500;'>"
                        ).text(item.sub_bid_kegiatan),
                        $(
                            "<td style='background:#FFFFFF;font-weight:500;'>"
                        ).html(
                            indikator + '&nbsp; <div id="isIndikator"><div>'
                        ),
                        $(
                            "<td style='background:#FFFFFF;font-weight:500;padding:0'>"
                        ).html(
                            '<table class="table" style="padding:0;margin-bottom:5px !important" id="showTargetSubBid' +
                            item.id_sub_bid +
                            '"></table><div align="center" id="subTargetSubBid"></div>'
                        ),
                        $(
                            "<td style='background:#FFFFFF;font-weight:500;font-size: 11px;text-align:center'>"
                        ).text(satuan),
                        $(
                            "<td style='background:#FFFFFF;font-weight:500;' align='center'>"
                        ).html('<div id="paguShow"></div>')
                    )
                    .insertAfter("#subkegiatan" + item.id_sub_kegiatan);
                if (item.pagu === null) {
                    $("#paguShow").html(
                        '<a href="javascript:void(0)" onclick="setIdSubBidang(' +
                        item.id_sub_bid +
                        ')" data-bs-toggle="modal" data-bs-target="#pagu"><svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="red" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><rect x="4" y="4" width="16" height="16" rx="2" /><line x1="9" y1="12" x2="15" y2="12" /><line x1="12" y1="9" x2="12" y2="15" /></svg></a>'
                    );
                } else {
                    $("#paguShow").html(item.pagu);
                }
                if (item.indikator === null) {
                    $("#isIndikator").html(
                        '<a href="javascript:void(0)" onclick="setIdSubBidangIndikator(' +
                        item.id_sub_bid +
                        ')" data-bs-toggle="modal" data-bs-target="#indikator-subbidang"><svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><rect x="4" y="4" width="16" height="16" rx="2" /><line x1="9" y1="12" x2="15" y2="12" /><line x1="12" y1="9" x2="12" y2="15" /></svg></a>'
                    );
                }

                var thn = item.tahun;
                var target = item.target;
                var result = thn.split(",");
                var resultTarget = target.split(",");

                if (thn === "") {
                    var divHtml = "<tr>";
                    for (var s = 0; s < result.length; s++) {
                        divHtml +=
                            "<td style='font-weight:500;font-size:10px;padding:5px;text-align:center'>" +
                            result[s] +
                            "</td>";
                    }
                    if (item.id_indikator_sub_bid_kegiatan !== null) {
                        divHtml +=
                            '<td rowspan="2" style="width:20px;"><div align="center"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#target" onclick="setPropTarget(' +
                            item.id_indikator_sub_bid_kegiatan +
                            ',\'proses-tambah-target-sub-bidang\')" ><svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><rect x="4" y="4" width="16" height="16" rx="2" /><line x1="9" y1="12" x2="15" y2="12" /><line x1="12" y1="9" x2="12" y2="15" /></svg></a></div></td></tr><tr>';
                    }
                    for (var tr = 0; tr < resultTarget.length; tr++) {
                        divHtml +=
                            "<td style='font-weight:500;font-size:10px;padding:0;text-align:center'>" +
                            resultTarget[tr] +
                            "</td>";
                    }
                    divHtml += "</tr>";
                } else {
                    for (var s = 0; s < result.length; s++) {
                        divHtml +=
                            "<tr><td style='font-weight:500;font-size:10px;padding:5px;text-align:center'>" +
                            result[s] +
                            "</td></tr>";
                    }
                    for (var tr = 0; tr < resultTarget.length; tr++) {
                        divHtml +=
                            "<tr><td style='font-weight:500;font-size:10px;padding:0;text-align:center'>" +
                            resultTarget[tr] +
                            "</td></tr>";
                    }
                }

                $("#showTargetSubBid" + item.id_sub_bid).append(divHtml);
            });
        },
    });
}

function setIdProgram(id) {
    $("#id_program").val(id);
}

function setIdKegiatan(id) {
    $("#id_kegiatan").val(id);
}

function setIdSubKegiatan(id) {
    $("#id_sub_kegiatan").val(id);
}

function setIdProgramIndikator(id) {
    $("#id_program_indikator").val(id);
}

function setIdKegiatanIndikator(id) {
    $("#id_kegiatan_indikator").val(id);
}

function setIdSubKegiatanIndikator(id) {
    $("#id_sub_kegiatan_indikator").val(id);
}

function setIdSubBidangIndikator(id) {
    $("#id_sub_bidang_indikator").val(id);
}

function setPropTarget(id, act) {
    $("#formTarget").attr("action", act);
    $("#id_indikator").val(id);
}

function setIdSubBidang(id) {
    $("#id_sub_bidang").val(id);
}

$(document).ready(function(e){
    loadData();
});
