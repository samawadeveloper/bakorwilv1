loadData();

function loadData() {

    $.ajax({
        processing: true,
        serverSide: true,
        type: "GET",
        url: "/realisasi-anggaran",
        dataType: "json",
        cache: false,
        success: function(obj) {
            $(".loading").css("display", "none");
            $(".dtable-realisasi-anggaran").DataTable({
                data: obj.data,
                columns: [{
                        data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                    },
                    {
                        render: function(data, type, row, meta) {
                            return row.program + "<br>" + row.kode_program;
                        },
                    },
                    {
                        render: function(data, type, row, meta) {
                            return row.kegiatan + "<br>" + row.kode_kegiatan;
                        },
                    },
                    {
                        render: function(data, type, row, meta) {
                            return row.sub_kegiatan + "<br>" + row.kode_sub_kegiatan;
                        },
                    },
                    {
                        render: function(data, type, row, meta) {
                            return row.sub_bid_kegiatan + "<br>" + row.kode_sub_kegiatan;
                        },
                    },
                    {
                        render: function(data, type, row, meta) {
                            return formatNumber(row.pagu);
                        },
                    },
                    { data: "action", name: "action", orderable: false },
                ],
                columnDefs: [{
                    targets: "no-sort",
                    orderable: false,
                }, ],
                oLanguage: {
                    sLengthMenu: "Menampilkan _MENU_ data",
                    sSearch: "Cari",
                    sInfo: "Menampilkan _START_ - _END_ dari _TOTAL_ data",
                    oPaginate: {
                        sFirst: "Pertama", // This is the link to the first page
                        sPrevious: "Sebelumnya", // This is the link to the previous page
                        sNext: "Selanjutnya", // This is the link to the next page
                        sLast: "Terakhir", // This is the link to the last page
                    },
                },
                responsive: true,
                bDestroy: true,
            });
        },
        error: function(obj, textstatus) {
            alert(obj.msg);
        },
    });

}

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}

function dataModalRealisasi(id, pagu) {
    $("#id_kegiatan_sub_bidang").val(id);
    $("#pagu_txt").html(formatNumber(pagu));
}

function dataModalRealisasiUpdate(id, id_realisasi, pagu, realisasi, keterangan) {
    $("#realisasi_update_txt").val(realisasi);
    $("#keterangna_update").val(keterangan);
    $("#id_realisasi_anggaran").val(id_realisasi);
    $("#pagu_txt_update").html(formatNumber(pagu));
}

$("#formRealisasi").submit(function(e) {

    $(".loading").css("display", "block");
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var id_kegiatan_sub_bidang = $("#id_kegiatan_sub_bidang").val();
    var realisasi = $("#realisasi_txt").val();

    if (realisasi === "") {
        $("#realisasi_txt").focus();
        $(".err_realisasi").html("Isi Realisasi!");
        $(".loading").css("display", "none");
    } else {
        $("#btnSubmitRealisasi").text("Process ...");
        $("#btnSubmitRealisasi").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("id_kegiatan_sub_bidang", id_kegiatan_sub_bidang);
        form_data.append("realisasi", realisasi);
        form_data.append("keterangan", $("#keterangan").val());

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: 'proses-realisasi',
            type: "POST",
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function(data) {
                $("#btnSubmitRealisasi").removeAttr("disabled");
                $("#btnSubmitRealisasi").text("Submit");
                $(".loading").css("display", "none");
                $("#realisasi").modal("hide");
                $("#id_sub_bidang").val("");
                $("#realisasi_txt").val("");
                $("#keterangan").val("");
                loadData();
            },
            error: function(xhr, status, error) {
                $("#btnSubmitRealisasi").removeAttr("disabled");
                $("#btnSubmitRealisasi").text("Submit");
                alert(xhr.responseText);
            },
        });
    }
});

$("#formRealisasiUpdate").submit(function(e) {
    $(".loading").css("display", "block");
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var id_realisasi_anggaran = $("#id_realisasi_anggaran").val();
    var realisasi = $("#realisasi_update_txt").val();

    if (realisasi === "") {
        $("#realisasi_update_txt").focus();
        $(".err_realisasi_update").html("Isi Realisasi!");
        $(".loading").css("display", "none");
    } else {
        $("#btnSubmitRealisasiUpdate").text("Process ...");
        $("#btnSubmitRealisasiUpdate").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("id_realisasi_anggaran", id_realisasi_anggaran);
        form_data.append("realisasi", realisasi);
        form_data.append("keterangan", $("#keterangna_update").val());

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: 'proses-realisasi-ubah',
            type: "POST",
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function(data) {
                $("#btnSubmitRealisasiUpdate").removeAttr("disabled");
                $("#btnSubmitRealisasiUpdate").text("Submit");
                $(".loading").css("display", "none");
                $("#realisasi-update").modal("hide");
                $("#id_realisasi_anggaran").val("");
                $("#realisasi_update_txt").val("");
                $("#keterangna_update").val("");
                loadData();
            },
            error: function(xhr, status, error) {
                $("#btnSubmitRealisasiUpdate").removeAttr("disabled");
                $("#btnSubmitRealisasiUpdate").text("Submit");
                alert(xhr.responseText);
            },
        });
    }
});