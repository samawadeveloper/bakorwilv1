console.log('load JS');
loadData();

function loadData() {
    $(".table-program tbody").html("");
    $.ajax({
        processing: true,
        serverSide: true,
        type: "GET",
        url: "/laporan-kegiatan",
        dataType: "json",
        cache: false,
        success: function(obj) {
            $.each(obj.data, function(i, item) {
                if (item.indikator === null) {
                    var indikator = "";
                    var satuan = "";
                } else {
                    var indikator = item.indikator;

                    var satuan = item.satuan;
                }
                $("<tr id='program" + item.id + "'>")
                    .append(
                        $(
                            "<td style='background:#e5e5e5;font-weight:bold;font-size: 11px;'>"
                        ).html(item.kode),
                        $(
                            "<td style='background:#e5e5e5;font-weight:bold;font-size: 11px;'>"
                        ).html(item.program),
                        $(
                            "<td style='background:#e5e5e5;font-weight:500;font-size: 11px;'>"
                        ).html(
                            '<span style="font-size:9px;">' +
                            indikator +
                            '</span> &nbsp; <div id="isIndikator"></div>'
                        ),
                        $(
                            "<td style='padding:0;background:#e5e5e5;font-weight:500;font-size: 11px;'>"
                        ).html(
                            '<table class="table" style="padding:0;margin-bottom:5px !important" id="showTarget' +
                            item.id +
                            '"></table><div id="subTargetProgra"></div>'
                        ),
                        $(
                            "<td style='background:#e5e5e5;font-weight:500;font-size: 11px;text-align:center'>"
                        ).text(""),
                        $(
                            "<td style='background:#e5e5e5;font-weight:500;font-size: 11px;text-align:center'>"
                        ).text(""),
                        $(
                            "<td style='background:#e5e5e5;font-weight:500;font-size: 11px;text-align:center'>"
                        ).text("")
                    )
                    .appendTo(".table-program tbody");
                var thn = item.tahun;
                var target = item.target;
                var result = thn.split(",");
                var resultTarget = target.split(",");
                divHtml = "";

                var targetProsenProgram = (item.total_realisasi / item.target_realisasi) * ((parseInt(item.target) / 100) * 100);

                for (var tr = 0; tr < resultTarget.length; tr++) {
                    divHtml +=
                        "<tr><td style='font-weight:500;font-size:10px;padding:0;text-align:center'>" +
                        Math.round(targetProsenProgram) + "% / " + resultTarget[tr] + "&nbsp;" + satuan +
                        "</td></tr>";
                }

                $("#showTarget" + item.id).append(divHtml);
            });

            $.each(obj.kegiatan, function(i, item) {
                if (item.indikator === null) {
                    var indikator = "";
                    var satuan = "";
                } else {
                    var indikator = item.indikator;

                    var satuan = item.satuan;
                }
                $("<tr id='kegiatan" + item.id_kegiatan + "'>")
                    .append(
                        $(
                            "<td style='background:#f2f2f2;font-weight:500;'>"
                        ).html(item.kode_program + "." + item.kode),
                        $(
                            "<td style='background:#f2f2f2;font-weight:500;'>"
                        ).html(item.kegiatan),
                        $(
                            "<td style='background:#f2f2f2;font-weight:500;'>"
                        ).html(
                            indikator +
                            '&nbsp; <div id="isIndikator"></div>'
                        ),
                        $(
                            "<td style='background:#f2f2f2;font-weight:500;padding:0'>"
                        ).html(
                            '<table class="table" style="padding:0;margin-bottom:5px !important" id="showTargetKeg' +
                            item.id_kegiatan +
                            '"></table>'
                        ),
                        $(
                            "<td style='background:#f2f2f2;font-weight:500;'>"
                        ).text(""),
                        $(
                            "<td style='background:#f2f2f2;font-weight:500;'>"
                        ).text(""),
                        $(
                            "<td style='background:#f2f2f2;font-weight:500;'>"
                        ).text("")
                    )
                    .insertAfter("#program" + item.id_program);

                var thn = item.tahun;
                var target = item.target;
                var result = thn.split(",");
                var resultTarget = target.split(",");
                var divHtml = "";
                // for (var s = 0; s < result.length; s++) {
                //     divHtml +=
                //         "<tr><td style='font-weight:500;font-size:10px;padding:5px;text-align:center'>" +
                //         result[s] +
                //         "</td></tr>";
                // }

                for (var tr = 0; tr < resultTarget.length; tr++) {
                    divHtml +=
                        "<tr><td style='font-weight:500;font-size:10px;padding:0;text-align:center'>" +
                        item.total_realisasi + " / " + resultTarget[tr] + "&nbsp;" + satuan +
                        "</td></tr>";
                }


                $("#showTargetKeg" + item.id_kegiatan).append(divHtml);
            });

            $.each(obj.sub_kegiatan, function(i, item) {
                if (item.indikator === null) {
                    var indikator = "";
                    var satuan = "";
                } else {
                    var indikator = item.indikator;

                    var satuan = item.satuan;
                }
                $("<tr id='subkegiatan" + item.id_sub_kegiatan + "'>")
                    .append(
                        $(
                            "<td style='background:#f9f9f9;font-weight:500;'>"
                        ).html(
                            item.kode_program +
                            "." +
                            item.kode_kegiatan +
                            "." +
                            item.kode
                        ),
                        $(
                            "<td style='background:#f9f9f9;font-weight:500;'>"
                        ).html(item.sub_kegiatan),
                        $(
                            "<td style='background:#f9f9f9;font-weight:500;'>"
                        ).html(
                            indikator +
                            '&nbsp; <div id="isIndikator"></iv>'
                        ),
                        $(
                            "<td style='background:#f9f9f9;font-weight:500;padding:0'>"
                        ).html(
                            '<table class="table" style="padding:0;margin-bottom:5px !important" id="showTargetSUbKeg' +
                            item.id_sub_kegiatan +
                            '"></table>'
                        ),
                        $(
                            "<td style='background:#f9f9f9;font-weight:500;'>"
                        ).text(""),
                        $(
                            "<td style='background:#f9f9f9;font-weight:500;'>"
                        ).text(""),
                        $(
                            "<td style='background:#f9f9f9;font-weight:500;'>"
                        ).text("")
                    )
                    .insertAfter("#kegiatan" + item.id_kegiatan);

                var thn = item.tahun;
                var target = item.target;
                var result = thn.split(",");
                var resultTarget = target.split(",");

                var divHtml = "";

                for (var tr = 0; tr < resultTarget.length; tr++) {
                    divHtml +=
                        "<tr><td style='font-weight:500;font-size:10px;padding:0;text-align:center'>" +
                        resultTarget[tr] + "&nbsp;" + satuan +
                        "</td></tr>";
                }

                // $("#showTargetSUbKeg" + item.id_sub_kegiatan).append(divHtml);
            });

            $.each(obj.sub_bidang, function(i, item) {
                if (item.indikator === null) {
                    var indikator = "";
                    var satuan = "";
                } else {
                    var indikator = item.indikator;

                    var satuan = item.satuan;
                }
                $("<tr>")
                    .append(
                        $(
                            "<td style='background:#FFFFFF;font-weight:500;'>"
                        ).html(
                            item.kode_program +
                            "." +
                            item.kode_kegiatan +
                            "." +
                            item.kode_sub_kegiatan +
                            "." +
                            item.kode
                        ),
                        $(
                            "<td style='background:#FFFFFF;font-weight:500;'>"
                        ).text(item.sub_bid_kegiatan),
                        $(
                            "<td style='background:#FFFFFF;font-weight:500;'>"
                        ).html(
                            indikator +
                            '&nbsp; <div id="isIndikator"><div>'
                        ),
                        $(
                            "<td style='background:#FFFFFF;font-weight:500;padding:0'>"
                        ).html(
                            '<div style="text-align:center" id="showTargetSubBid' +
                            item.id_sub_bid +
                            '"></div>'
                        ),

                        $(
                            "<td style='background:#FFFFFF;font-weight:500;'>"
                        ).html(item.deskripsi_realisasi),
                        $(
                            "<td style='background:#FFFFFF;font-weight:500;' align='center'>"
                        ).html('<div id="paguShow"></div>'),
                        $(
                            "<td style='background:#FFFFFF;font-weight:500;'>"
                        ).html(formatNumber(item.realisasi_anggaran)),
                    )
                    .insertAfter("#subkegiatan" + item.id_sub_kegiatan);
                if (item.pagu === null) {
                    $("#paguShow").html('-');
                } else {
                    $("#paguShow").html(formatNumber(item.pagu));
                }

                var thn = item.tahun;
                var target = item.target;
                var result = thn.split(",");
                var resultTarget = target.split(",");
                for (var tr = 0; tr < resultTarget.length; tr++) {
                    divHtml = "<span style='text-align:center'>" + item.total_realisasi + "/" + resultTarget[tr] + "&nbsp;" + satuan + "</span>";
                }

                $("#showTargetSubBid" + item.id_sub_bid).append(divHtml);
            });
        },
    });
}

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
}