$("#formUbah").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var kode = $("#kode").val();
    var id = $("#id").val();
    var kegiatan = $("#kegiatan").val();
    var subkegiatan = $("#subkegiatan").val();


    if (kegiatan === "") {
        $("#kegiatan").focus();
        $(".err_kegiatan").html("Isi Kegiatan!");
    } else if (kode === "") {
        $("#kode").focus();
        $(".err_kode").html("Isi Kode!");
    } else if (subkegiatan === "") {
        $("#subkegiatan").focus();
        $(".err_subkegiatan").html("Isi Sub Kegiatan!");
    } else {
        $("#btnSubmit").text("Process ...");
        $("#btnSubmit").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("id", id);
        form_data.append("kode", kode);
        form_data.append("subkegiatan", subkegiatan);
        form_data.append("kegiatan", kegiatan);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/proses-ubah-sub-kegiatan',
            type: 'POST',
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function() {
                $("#btnSubmit").removeAttr('disabled');
                $("#btnSubmit").text('Submit');

                location.href = "/sub-kegiatan";

            },
            error: function(xhr, status, error) {
                $("#btnSubmit").removeAttr('disabled');
                $("#btnSubmit").text('Submit');
                alert(xhr.responseText);
            }
        });

    }
});