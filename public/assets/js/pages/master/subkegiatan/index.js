console.log("loaded Pengguna Source Js");

loadData();

function loadData() {

    $.ajax({
        processing: true,
        serverSide: true,
        type: "GET",
        url: "/sub-kegiatan",
        dataType: "json",
        cache: false,
        success: function(obj) {
            $(".loading").css("display", "none");
            $(".dtable-sub-kegiatan").DataTable({
                data: obj.data,
                columns: [{
                        render: function(data, type, row, meta) {
                            return row.kode_program + "." + row.kode_kegiatan + "." + row.kode;
                        },
                    },
                    {
                        render: function(data, type, row, meta) {
                            return row.sub_kegiatan;
                        },
                    },
                    { data: "action", name: "action", orderable: false },
                ],
                columnDefs: [{
                    targets: "no-sort",
                    orderable: false,
                }, ],
                responsive: true,
                bDestroy: true,
            });
        },
        error: function(obj, textstatus) {
            alert(obj.msg);
        },
    });

}