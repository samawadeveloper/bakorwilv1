loadData();

function loadData() {

    $.ajax({
        processing: true,
        serverSide: true,
        type: "GET",
        url: "/indikator-program/" + $("#program-id").html(),
        dataType: "json",
        cache: false,
        success: function(obj) {
            $(".loading").css("display", "none");
            $(".dtable-indikator").DataTable({
                data: obj.data,
                columns: [{
                        render: function(data, type, row, meta) {
                            return row.indikator;
                        },
                    },
                    {
                        render: function(data, type, row, meta) {
                            return row.satuan;
                        },
                    },
                    { data: "action", name: "action", orderable: false },
                ],
                columnDefs: [{
                    targets: "no-sort",
                    orderable: false,
                }, ],
                responsive: true,
                bDestroy: true,
            });
        },
        error: function(obj, textstatus) {
            alert(obj.msg);
        },
    });

}