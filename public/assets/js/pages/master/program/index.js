console.log("loaded Pengguna Source Js");

loadData();

function loadData() {

    $.ajax({
        processing: true,
        serverSide: true,
        type: "GET",
        url: "/program",
        dataType: "json",
        cache: false,
        success: function(obj) {
            $(".loading").css("display", "none");
            $(".dtable-program").DataTable({
                data: obj.data,
                columns: [{
                        render: function(data, type, row, meta) {
                            return row.kode;
                        },
                    },
                    {
                        render: function(data, type, row, meta) {
                            return row.program;
                        },
                    },
                    { data: "action", name: "action", orderable: false },
                ],
                columnDefs: [{
                    targets: "no-sort",
                    orderable: false,
                }, ],
                responsive: true,
                bDestroy: true,
            });
        },
        error: function(obj, textstatus) {
            alert(obj.msg);
        },
    });

}