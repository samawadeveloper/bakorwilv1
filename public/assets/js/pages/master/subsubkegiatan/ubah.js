$("#formUbah").submit(function(e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.
    var kode = $("#kode").val();
    var id = $("#id").val();
    var kegiatan = $("#kegiatan").val();
    var subkegiatan = $("#subkegiatan").val();
    var subsubkegiatan = $("#subsubkegiatan").val();
    var subbidang = $("#subbidang").val();
    var program = $("#program").val();


    if (program === "") {
        $("#program").focus();
        $(".err_program").html("Pilih Program!");
    } else if (kegiatan === "") {
        $("#kegiatan").focus();
        $(".err_kegiatan").html("Pilih Kegiatan!");
    } else if (subkegiatan === "") {
        $("#subkegiatan").focus();
        $(".err_subkegiatan").html("Pilih Sub Kegiatan!");
    } else if (subsubkegiatan === "") {
        $("#subsubkegiatan").focus();
        $(".err_subsubkegiatan").html("Pilih Sub Sub Kegiatan!");
    } else if (kode === "") {
        $("#kode").focus();
        $(".err_kode").html("Isi Kode!");
    } else if (subbidang === "") {
        $("#subbidang").focus();
        $(".err_subbidang").html("Pilih Sub Bidang!");
    } else {
        $("#btnSubmit").text("Process ...");
        $("#btnSubmit").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("kode", kode);
        form_data.append("subsubkegiatan", subsubkegiatan);
        form_data.append("subkegiatan", subkegiatan);
        form_data.append("subbidang", subbidang);
        form_data.append("id", id);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/proses-ubah-sub-sub-kegiatan',
            type: 'POST',
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function() {
                $("#btnSubmit").removeAttr('disabled');
                $("#btnSubmit").text('Submit');

                location.href = "/sub-sub-kegiatan";

            },
            error: function(xhr, status, error) {
                $("#btnSubmit").removeAttr('disabled');
                $("#btnSubmit").text('Submit');
                alert(xhr.responseText);
            }
        });

    }
});