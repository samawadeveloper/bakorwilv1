$("#formAdd").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var kode = $("#kode").val();
    var program = $("#program").val();
    var kegiatan = $("#kegiatan").val();


    if (program === "") {
        $("#program").focus();
        $(".err_program").html("Isi Program!");
    } else if (kode === "") {
        $("#kode").focus();
        $(".err_kode").html("Isi Kode!");
    } else if (kegiatan === "") {
        $("#kegiatan").focus();
        $(".err_kegiatan").html("Isi Kegiatan!");
    } else {
        $("#btnSubmit").text("Process ...");
        $("#btnSubmit").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("kode", kode);
        form_data.append("program", program);
        form_data.append("kegiatan", kegiatan);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: 'proses-tambah-kegiatan',
            type: 'POST',
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function() {
                $("#btnSubmit").removeAttr('disabled');
                $("#btnSubmit").text('Submit');

                location.href = "/kegiatan";

            },
            error: function(xhr, status, error) {
                $("#btnSubmit").removeAttr('disabled');
                $("#btnSubmit").text('Submit');
                alert(xhr.responseText);
            }
        });

    }
});