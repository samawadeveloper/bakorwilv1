$("#formAdd").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var indikator = $("#indikator").val();
    var satuan = $("#satuan").val();
    var id = $("#id").val();
    var bidang = $("#bidang").val();

    if (indikator === "") {
        $("#indikator").focus();
        $(".err_indikator").html("Isi Indikator!");
    } else if (satuan === "") {
        $("#satuan").focus();
        $(".err_satuan").html("Isi Satuan!");
    } else if (bidang === "") {
        $("#bidang").focus();
        $(".err_bidang").html("Pilih Bidang!");
    } else {
        $("#btnSubmit").text("Process ...");
        $("#btnSubmit").attr("disabled", "disabled");

        var form_data = new FormData();
        form_data.append("indikator", indikator);
        form_data.append("bidang", bidang);
        form_data.append("satuan", satuan);
        form_data.append("id", id);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/proses-tambah-indikator-kegiatan',
            type: 'POST',
            contentType: false,
            processData: false,
            data: form_data, // serializes the form's elements.

            success: function() {
                $("#btnSubmit").removeAttr('disabled');
                $("#btnSubmit").text('Submit');

                location.href = "/indikator-kegiatan/" + id;

            },
            error: function(xhr, status, error) {
                $("#btnSubmit").removeAttr('disabled');
                $("#btnSubmit").text('Submit');
                alert(xhr.responseText);
            }
        });

    }
});