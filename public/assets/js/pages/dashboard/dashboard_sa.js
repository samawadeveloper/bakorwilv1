console.log("Dashboard_sa.js Loaded!");

loadData();

function loadData() {

    $.ajax({
        processing: true,
        serverSide: true,
        type: "GET",
        url: "/pengguna",
        dataType: "json",
        cache: false,
        success: function(obj) {
            $(".loading").css("display", "none");
            $(".dtable-pengguna").DataTable({
                data: obj.data,
                columns: [{
                        data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                    },
                    {
                        render: function(data, type, row, meta) {
                            return row.nama;
                        },
                    },
                    {
                        render: function(data, type, row, meta) {
                            if (row.aktif === 1) {
                                return "Aktif";
                            } else {
                                return "Non Aktif";
                            }
                        },
                    },
                    { data: "action", name: "action", orderable: false },
                ],
                columnDefs: [{
                    targets: "no-sort",
                    orderable: false,
                    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']]
                }],
                oLanguage: {
                    sLengthMenu: "Menampilkan _MENU_ data",
                    sSearch: "Cari",
                    sInfo: "Menampilkan _START_ - _END_ dari _TOTAL_ data",
                    oPaginate: {
                        sFirst: "Pertama", // This is the link to the first page
                        sPrevious: "Sebelumnya", // This is the link to the previous page
                        sNext: "Selanjutnya", // This is the link to the next page
                        sLast: "Terakhir", // This is the link to the last page
                    },
                },
                responsive: true,
                bDestroy: true,
            });
        },
        error: function(obj, textstatus) {
            alert(obj.msg);
        },
    });

}
