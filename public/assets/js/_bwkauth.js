$(document).ready(function() {
    $("#formLoginAdmin").submit(function() {
        var username = $("#username").val();
        var password = $("#password").val();
        $(".feedback-login").empty();
        if (username !== "" && password !== "") {
            if (!validateEmail(username)) {
                $("#username").focus();
                $(".feedback-login").html("Username Tidak Valid");
            } else {
                $("#btn-login").text("Memulai..");
                $("#btn-login").attr("disabled", "disabled");
                setTimeout(function() {
                    $.ajax({
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                                "content"
                            ),
                        },
                        type: "POST",
                        data: {
                            username: username,
                            password: password,
                        },
                        url: "/auth-login-admin",
                        beforeSend: function() {
                            $("#btn-login").text("Proses Masuk..");
                        },
                        success: function(data) {
                            if (data.error === "false") {
                                window.location.href = "/dashboard";
                            } else {
                                $(".feedback-login").html(
                                    "Upaya Masuk Gagal, Ulangi Lagi"
                                );
                                $("#btn-login").text("Login");
                                $("#btn-login").removeAttr("disabled");
                            }
                        },
                        error: function(xhr) {
                            // if error occured
                            $(".feedback-login").html(xhr.responseText);
                            $("#btn-login").text("Login");
                            $("#btn-login").removeAttr("disabled");
                        },
                        complete: function() {
                            $("#btn-login").text("Login");
                            $("#btn-login").removeAttr("disabled");
                        },
                        dataType: "json",
                        cache: false,
                    });
                }, 650);
            }
        } else {
            if (username === "") {
                $("#username").focus();
                $(".feedback-login").append(
                    '<i class="ft-alert-circle"></i> Masukan Username!'
                );
            } else if (password === "") {
                $("#password").focus();

                $(".feedback-login").html("Masukan Password!");
            } else {
                $("#username").focus();

                $(".feedback-login").html("Masukan Username & Password!");
            }
        }

        return false;
    });

    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
    }
});