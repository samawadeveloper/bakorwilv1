<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class MasterSubKegiatanController extends Controller
{
    public function index(){
        if(request()->ajax()) {
            $kegiatan = DB::table('bkw_3_1_kegiatan_sub')
            ->join('bkw_2_1_kegiatan','bkw_3_1_kegiatan_sub.id_kegiatan','=','bkw_2_1_kegiatan.id')
            ->join('bkw_1_1_program','bkw_2_1_kegiatan.id_program','=','bkw_1_1_program.id')
            ->select('bkw_2_1_kegiatan.kode as kode_kegiatan','bkw_1_1_program.kode as kode_program','bkw_3_1_kegiatan_sub.kode','bkw_3_1_kegiatan_sub.sub_kegiatan','bkw_3_1_kegiatan_sub.id')->get();
            return Datatables::of($kegiatan)
                ->addColumn('action', function ($kegiatan) {
                    return '<a href="/ubah-sub-kegiatan/'.$kegiatan->id.'" class="btn btn-sm btn-outline-primary">Ubah</a> <a href="/indikator-sub-kegiatan/'.$kegiatan->id.'" class="btn btn-sm btn-outline-success">Indikator</a>';
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('master.subkegiatan.index');
    }

    public function tambah(){
        $get = DB::table('bkw_1_1_program')->get();
        $data = array(
            'program' => $get
        );
        return view('master.subkegiatan.tambah')->with($data);
    }
    public function prosesTambah(Request $request){
        $insert = DB::table('bkw_3_1_kegiatan_sub')->insert([
            'kode' => $request->kode,
            'id_kegiatan' => $request->kegiatan,
            'sub_kegiatan' => $request->subkegiatan,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD SUB KEGIATAN',
            'deskripsi' => 'Melakukan Aktifitas Menambah SUB KEGIATAN ' . $request->subkegiatan,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }
    public function ubah($id){
        $get = DB::table('bkw_3_1_kegiatan_sub')
            ->join('bkw_2_1_kegiatan','bkw_3_1_kegiatan_sub.id_kegiatan','=','bkw_2_1_kegiatan.id')
            ->join('bkw_1_1_program','bkw_2_1_kegiatan.id_program','=','bkw_1_1_program.id')
            ->select('bkw_2_1_kegiatan.id as id_kegiatan','bkw_1_1_program.id as id_program','bkw_3_1_kegiatan_sub.kode','bkw_3_1_kegiatan_sub.sub_kegiatan','bkw_3_1_kegiatan_sub.id')
            ->where('bkw_3_1_kegiatan_sub.id', $id)
            ->first();

        $program = DB::table('bkw_1_1_program')->get();
        $kegiatan = DB::table('bkw_2_1_kegiatan')->get();
        $data = array(
            'program' => $program,
            'kegiatan' => $kegiatan,
            'data' => $get,
        );
        return view('master.subkegiatan.ubah')->with($data);
    }

    public function prosesUbah(Request $request){
        $insert = DB::table('bkw_3_1_kegiatan_sub')->where('id', $request->id)->update([
            'kode' => $request->kode,
            'id_kegiatan' => $request->kegiatan,
            'sub_kegiatan' => $request->subkegiatan,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => Session::get('sessionIdAdmin'),
        ]);

        DB::table('bkw_t_log')->insert([
            'nama_log' => 'UBAH SUB KEGIATAN',
            'deskripsi' => 'Melakukan Aktifitas Mengubah SUB KEGIATAN ' . $request->subkegiatan,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }

    public function getKegiatan($id){
        $getData = DB::table('bkw_2_1_kegiatan')->where('id_program', $id)->get();

        $html = '<option value="">Pilih Kegiatan</option>';
        foreach ($getData as $cmbItem) {
            $html .= '<option value="'.$cmbItem->id.'">'.strtoupper($cmbItem->kode).'</option>';
        }

        if($getData){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
                'data' => $html,
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }
    public function indikator($id){
        if(request()->ajax()) {
            $program = DB::table('bkw_3_2_indikator_kegiatan_sub')->where('id_kegiatan_sub', $id)->get();
            return Datatables::of($program)
                ->addColumn('action', function ($program) {
                    return '<a href="/ubah-indikator-sub-kegiatan/'.$program->id.'" class="btn btn-sm btn-outline-primary">Ubah</a> <a href="/target-sub-kegiatan/'.$program->id.'" class="btn btn-sm btn-outline-danger">Target</a>';
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        $getNameProgram = DB::table('bkw_3_1_kegiatan_sub')->where('id', $id)->first();
        $data = array(
            'id' => $id,
            'sub_kegiatan' => $getNameProgram->sub_kegiatan,
            'kode' => $getNameProgram->kode,
        );
        return view('master.subkegiatan.indikator')->with($data);
    }

    public function tambahIndikator($id){
        $data=array(
            'id' => $id,
            
        );
        return view('master.subkegiatan.tambahIndikator')->with($data);
    }
    public function ubahIndikator($id){
        
        $d = DB::table('bkw_3_2_indikator_kegiatan_sub')->where('id', $id)->first();
        $data=array(
            'id' => $id,
            'd' => $d,            
        );
        return view('master.subkegiatan.ubahIndikator')->with($data);
    }

    public function prosesTambahIndikator(Request $request){
        $insert = DB::table('bkw_3_2_indikator_kegiatan_sub')->insert([
            'id_kegiatan_sub' => $request->id,
            'indikator' => $request->indikator,
            'satuan' => $request->satuan,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }
    public function prosesUbahIndikator(Request $request){
        $getIdProgram = DB::table('bkw_3_2_indikator_kegiatan_sub')->where('id', $request->id)->first();
        $insert = DB::table('bkw_3_2_indikator_kegiatan_sub')->where('id', $request->id)->update([
            'indikator' => $request->indikator,
            'satuan' => $request->satuan,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
                'id' => $getIdProgram->id_kegiatan_sub
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }
}
