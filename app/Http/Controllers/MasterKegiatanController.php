<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class MasterKegiatanController extends Controller
{
    public function index(){
        if(request()->ajax()) {
            $kegiatan = DB::table('bkw_2_1_kegiatan')->join('bkw_1_1_program','bkw_2_1_kegiatan.id_program','=','bkw_1_1_program.id')->select('bkw_1_1_program.kode as kode_program','bkw_2_1_kegiatan.kode','bkw_2_1_kegiatan.kegiatan','bkw_2_1_kegiatan.id')->get();
            return Datatables::of($kegiatan)
                ->addColumn('action', function ($kegiatan) {
                    return '<a href="/ubah-kegiatan/'.$kegiatan->id.'" class="btn btn-sm btn-outline-primary">Ubah</a> <a href="/indikator-kegiatan/'.$kegiatan->id.'" class="btn btn-sm btn-outline-success">Indikator</a>';
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('master.kegiatan.index');
    }

    public function tambah(){
        $get = DB::table('bkw_1_1_program')->get();
        
        $data = array(
            'program' => $get,
            
        );
        return view('master.kegiatan.tambah')->with($data);
    }

    public function prosesTambah(Request $request){
        $insert = DB::table('bkw_2_1_kegiatan')->insert([
            'kode' => $request->kode,
            'id_program' => $request->program,
            'kegiatan' => $request->kegiatan,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD KEGIATAN',
            'deskripsi' => 'Melakukan Aktifitas menambah KEGIATAN ' . $request->kegiatan,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
                'data' => $request->program
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }
    
    public function ubah($id){
        $program = DB::table('bkw_1_1_program')->get();
        $get = DB::table('bkw_2_1_kegiatan')->where('id', $id)->first();
        $data = array(
            'data' => $get,
            'program' => $program
        );

        return view('master.kegiatan.ubah')->with($data);
    }
    
    public function prosesUbah(Request $request){
        $update = DB::table('bkw_2_1_kegiatan')->where('id', $request->id)->update([
            'id_program' => $request->program,
            'kode' => $request->kode,
            'kegiatan' => $request->kegiatan,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => Session::get('sessionIdAdmin'),
        ]);
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'UBAH KEGIATAN',
            'deskripsi' => 'Melakukan Aktifitas mengubah KEGIATAN ' . $request->kegiatan,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($update){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }

    public function indikator($id){
        if(request()->ajax()) {
            $program = DB::table('bkw_2_2_indikator_kegiatan')->where('id_kegiatan', $id)->get();
            return Datatables::of($program)
                ->addColumn('action', function ($program) {
                    return '<a href="/ubah-indikator-kegiatan/'.$program->id.'" class="btn btn-sm btn-outline-primary">Ubah</a> <a href="/target-kegiatan/'.$program->id.'" class="btn btn-sm btn-outline-danger">Target</a>';
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        $getNameProgram = DB::table('bkw_2_1_kegiatan')->where('id', $id)->first();
        $data = array(
            'id' => $id,
            'kegiatan' => $getNameProgram->kegiatan,
            'kode' => $getNameProgram->kode,
        );
        return view('master.kegiatan.indikator')->with($data);
    }

    public function tambahIndikator($id){
        $bd = DB::table('bkw_master_bidang')->get();
        $data=array(
            'id' => $id,
            'bd' => $bd,
            
        );
        return view('master.kegiatan.tambahIndikator')->with($data);
    }
    public function ubahIndikator($id){
        $bd = DB::table('bkw_master_bidang')->get();
        $d = DB::table('bkw_2_2_indikator_kegiatan')->where('id', $id)->first();
        $data=array(
            'id' => $id,
            'd' => $d,
            'bd' => $bd,
            
        );
        return view('master.kegiatan.ubahIndikator')->with($data);
    }

    public function prosesTambahIndikator(Request $request){
        $insert = DB::table('bkw_2_2_indikator_kegiatan')->insert([
            'id_kegiatan' => $request->id,
            'id_bidang' => $request->bidang,
            'indikator' => $request->indikator,
            'satuan' => $request->satuan,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }
    public function prosesUbahIndikator(Request $request){
        $getIdProgram = DB::table('bkw_2_2_indikator_kegiatan')->where('id', $request->id)->first();
        $insert = DB::table('bkw_2_2_indikator_kegiatan')->where('id', $request->id)->update([
            'indikator' => $request->indikator,
            'id_bidang' => $request->bidang,
            'satuan' => $request->satuan,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
                'id' => $getIdProgram->id_kegiatan
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }

}
