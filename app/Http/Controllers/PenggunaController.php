<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class PenggunaController extends Controller
{
    public function index(){
        if(request()->ajax()) {
            $user = DB::table('bkw_master_user')
            ->leftJoin('bkw_master_sub_bidang','bkw_master_user.id_posisi','=','bkw_master_sub_bidang.id')
            ->select('bkw_master_user.id','bkw_master_user.username','bkw_master_user.nama','bkw_master_user.status','bkw_master_user.aktif','bkw_master_sub_bidang.sub_bid')
            ->get();
            return Datatables::of($user)
                ->addColumn('action', function ($user) {
                    if($user->aktif == 1){
                        $actStatus = 'Non Aktifkan';
                        $classActStatus = 'btn-danger';
                    }else{
                        $actStatus = 'Aktifkan';
                        $classActStatus = 'btn-success';
                    }
                    return '<a href="/ubah-pengguna/'.$user->id.'" class="btn btn-info btn-sm">Ubah</a> <a href="javascript:void(0)"  data-bs-toggle="modal" data-bs-target="#delModalPengguna" onclick="showModalPengguna('.$user->id.',\''.$user->nama.'\',\''.$actStatus.'\')" class="btn '.$classActStatus.' btn-sm">'.$actStatus.'</a>';
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('pengguna.index');
    }

    public function tambah(){
        $level = DB::table('bkw_master_level')->get();
        $data = array(
            'level' => $level
        );
        return view('pengguna.tambah')->with($data);
    }

    public function getSubBid(){
        $getData = DB::table('bkw_master_sub_bidang')->get();

        $html = '';
        foreach ($getData as $cmbItem) {
            $html .= '<option value="'.$cmbItem->id.'">'.strtoupper($cmbItem->sub_bid).'</option>';
        }

        if($getData){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
                'data' => $html,
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }

    public function prosesTambahPengguna(Request $request){
        if($request->status === '5'){
            $id_posisi = $request->sub_bid;
        }else{
            $id_posisi = '';
        }
        $insert = DB::table('bkw_master_user')->insertGetId([
            'nama' => $request->nama,
            'username' => $request->username,
            'password' => md5($request->password),
            'status' => $request->status,
            'aktif' => 1,
            'id_posisi' => $id_posisi,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        //Catat Log
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD PENGGUNA',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan pengguna Dengan Username ' . $request->username,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }

    public function prosesUbahPengguna(Request $request){
        if($request->status === '5'){
            $id_posisi = $request->sub_bid;
        }else{
            $id_posisi = '';
        }

        if($request->password === ''){
            $getPass = DB::table('bkw_master_user')->where('id', $request->id_user)->first();
            $password = $getPass->password;
        }else{
            $password = md5($request->password);
        }

        $update = DB::table('bkw_master_user')->where('id', $request->id_user)->update([
            'nama' => $request->nama,
            'username' => $request->username,
            'password' => $password,
            'status' => $request->status,
            'id_posisi' => $id_posisi,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => Session::get('sessionIdAdmin'),
        ]);

        //Catat Log
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'UBAH PENGGUNA',
            'deskripsi' => 'Melakukan Aktifitas Mengubah pengguna Dengan Username ' . $request->username,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($update){
            return response()->json([
                'error' => 'false',
                'message' => 'Update Berhasil',
            ], 200);
        }else{
            return response()->json('Update Gagal', 401);
        }
    }

    public function ubah($id){
        $getData = DB::table('bkw_master_user')->where('id', $id)->first();
        $level = DB::table('bkw_master_level')->get();
        $getSubBid = DB::table('bkw_master_sub_bidang')->get();
        $data = array(
            'data' => $getData,
            'level' => $level,
            'subBid' => $getSubBid
        );

        return view('pengguna.ubah')->with($data);
    }

    public function prosesAktifasiPengguna($id){
        $getStatusCurrent = DB::table('bkw_master_user')->select('aktif','username')->where('id', $id)->first();

        if($getStatusCurrent->aktif == 1){
            $changeStatus = 0;
        }else{
            $changeStatus = 1;
        }

        $update = DB::table('bkw_master_user')->where('id', $id)->update([
            'aktif' => $changeStatus,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => Session::get('sessionIdAdmin'),
        ]);

        //Catat Log
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'AKTIFASI PENGGUNA',
            'deskripsi' => 'Melakukan Aktifitas Mengubah aktifasi ke '. $changeStatus .' Dengan Username ' . $getStatusCurrent->username,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($update){
            return response()->json([
                'error' => 'false',
                'message' => 'Update Berhasil',
            ], 200);
        }else{
            return response()->json('Update Gagal', 401);
        }
    }

}
