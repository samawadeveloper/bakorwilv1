<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class MasterSubSubKegiatanController extends Controller
{
    public function index(){
        if(request()->ajax()) {
            $kegiatan = DB::table('bkw_4_1_kegiatan_sub_bid')
            ->join('bkw_3_1_kegiatan_sub','bkw_4_1_kegiatan_sub_bid.id_kegiatan_sub','=','bkw_3_1_kegiatan_sub.id')
            ->join('bkw_2_1_kegiatan','bkw_3_1_kegiatan_sub.id_kegiatan','=','bkw_2_1_kegiatan.id')
            ->join('bkw_1_1_program','bkw_2_1_kegiatan.id_program','=','bkw_1_1_program.id')
            ->select('bkw_4_1_kegiatan_sub_bid.id','bkw_4_1_kegiatan_sub_bid.kode','bkw_4_1_kegiatan_sub_bid.sub_bid_kegiatan','bkw_3_1_kegiatan_sub.kode as kode_sub_kegiatan','bkw_2_1_kegiatan.kode as kode_kegiatan','bkw_1_1_program.kode as kode_program','bkw_3_1_kegiatan_sub.kode as kode_sub_kegiatan')->get();
            return Datatables::of($kegiatan)
                ->addColumn('action', function ($kegiatan) {
                    return '<a href="/ubah-sub-sub-kegiatan/'.$kegiatan->id.'" class="btn btn-sm btn-outline-primary">Ubah</a> <a href="/indikator-sub-sub-kegiatan/'.$kegiatan->id.'" class="btn btn-sm btn-outline-success">Indikator</a>';
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('master.subsubkegiatan.index');
    }
    public function tambah(){
        $get = DB::table('bkw_1_1_program')->get();
        $sub_bidang = DB::table('bkw_master_sub_bidang')->get();
        $data = array(
            'program' => $get,
            'sub_bidang' => $sub_bidang,
        );
        return view('master.subsubkegiatan.tambah')->with($data);
    }
    public function ubah($id){
        $get = DB::table('bkw_4_1_kegiatan_sub_bid')
        ->join('bkw_3_1_kegiatan_sub','bkw_4_1_kegiatan_sub_bid.id_kegiatan_sub','=','bkw_3_1_kegiatan_sub.id')
        ->join('bkw_2_1_kegiatan','bkw_3_1_kegiatan_sub.id_kegiatan','=','bkw_2_1_kegiatan.id')
        ->join('bkw_1_1_program','bkw_2_1_kegiatan.id_program','=','bkw_1_1_program.id')
        ->select('bkw_4_1_kegiatan_sub_bid.id_sub_bidang','bkw_4_1_kegiatan_sub_bid.id','bkw_4_1_kegiatan_sub_bid.kode','bkw_4_1_kegiatan_sub_bid.sub_bid_kegiatan','bkw_3_1_kegiatan_sub.id as id_sub_kegiatan','bkw_2_1_kegiatan.id as id_kegiatan','bkw_1_1_program.id as id_program')
        ->where('bkw_4_1_kegiatan_sub_bid.id', $id)
        ->first();

        $program = DB::table('bkw_1_1_program')->get();
        $kegiatan = DB::table('bkw_2_1_kegiatan')->get();
        $subkegiatan = DB::table('bkw_3_1_kegiatan_sub')->get();
        $sub_bidang = DB::table('bkw_master_sub_bidang')->get();
        $data = array(
            'program' => $program,
            'kegiatan' => $kegiatan,
            'subkegiatan' => $subkegiatan,
            'subbidang' => $sub_bidang,
            'data' => $get,
        );
        return view('master.subsubkegiatan.ubah')->with($data);
    }
    public function getSubKegiatan($id){
        $getData = DB::table('bkw_3_1_kegiatan_sub')->where('id_kegiatan', $id)->get();

        $html = '<option value="">Pilih Sub Kegiatan</option>';
        foreach ($getData as $cmbItem) {
            $html .= '<option value="'.$cmbItem->id.'">'.strtoupper($cmbItem->kode).'</option>';
        }

        if($getData){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
                'data' => $html,
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }

    public function prosesTambah(Request $request){
        $subkegiatan = $request->subkegiatan;
        $kode = $request->kode;
        $subsubkegiatan = $request->subsubkegiatan;
        $subbidang = $request->subbidang;

        $insert = DB::table('bkw_4_1_kegiatan_sub_bid')
                    ->insert([
                      'kode' => $kode,  
                      'id_sub_bidang' => $subbidang,  
                      'id_kegiatan_sub' => $subkegiatan,  
                      'sub_bid_kegiatan' => $subsubkegiatan,  
                      'input_at' => date('Y-m-d H:i:s'),
                        'input_by' => Session::get('sessionIdAdmin'),
                    ]);
        
                    DB::table('bkw_t_log')->insert([
                        'nama_log' => 'ADD SUB SUB KEGIATAN',
                        'deskripsi' => 'Melakukan Aktifitas Menambahkan SUB SUB KEGIATAN ' . $subsubkegiatan,
                        'log_at' => date('Y-m-d H:i:s'),
                        'log_by' => Session::get('sessionIdAdmin'),
                    ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }
    public function prosesUbah(Request $request){
        $id = $request->id;
        $subkegiatan = $request->subkegiatan;
        $kode = $request->kode;
        $subsubkegiatan = $request->subsubkegiatan;
        $subbidang = $request->subbidang;

        $insert = DB::table('bkw_4_1_kegiatan_sub_bid')
                    ->where('id', $id)
                    ->update([
                      'kode' => $kode,  
                      'id_sub_bidang' => $subbidang,  
                      'id_kegiatan_sub' => $subkegiatan,  
                      'sub_bid_kegiatan' => $subsubkegiatan,  
                      'update_at' => date('Y-m-d H:i:s'),
                        'update_by' => Session::get('sessionIdAdmin'),
                    ]);
        
                    DB::table('bkw_t_log')->insert([
                        'nama_log' => 'UBAH SUB SUB KEGIATAN',
                        'deskripsi' => 'Melakukan Aktifitas Mengubah SUB SUB KEGIATAN ' . $subsubkegiatan,
                        'log_at' => date('Y-m-d H:i:s'),
                        'log_by' => Session::get('sessionIdAdmin'),
                    ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }
    public function indikator($id){
        if(request()->ajax()) {
            $program = DB::table('bkw_4_2_indikator_kegiatan_sub_bid')->where('id_kegiatan_sub_bid', $id)->get();
            return Datatables::of($program)
                ->addColumn('action', function ($program) {
                    return '<a href="/ubah-indikator-sub-sub-kegiatan/'.$program->id.'" class="btn btn-sm btn-outline-primary">Ubah</a> <a href="/target-sub-sub-kegiatan/'.$program->id.'" class="btn btn-sm btn-outline-danger">Target</a>';
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        $getNameProgram = DB::table('bkw_4_1_kegiatan_sub_bid')->where('id', $id)->first();
        $data = array(
            'id' => $id,
            'sub_sub_kegiatan' => $getNameProgram->sub_bid_kegiatan,
            'kode' => $getNameProgram->kode,
        );
        return view('master.subsubkegiatan.indikator')->with($data);
    }

    public function tambahIndikator($id){
        $data=array(
            'id' => $id,
            
        );
        return view('master.subsubkegiatan.tambahIndikator')->with($data);
    }
    public function ubahIndikator($id){
        
        $d = DB::table('bkw_4_2_indikator_kegiatan_sub_bid')->where('id', $id)->first();
        $data=array(
            'id' => $id,
            'd' => $d,            
        );
        return view('master.subsubkegiatan.ubahIndikator')->with($data);
    }

    public function prosesTambahIndikator(Request $request){
        $insert = DB::table('bkw_4_2_indikator_kegiatan_sub_bid')->insert([
            'id_kegiatan_sub_bid' => $request->id,
            'indikator' => $request->indikator,
            'satuan' => $request->satuan,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }
    public function prosesUbahIndikator(Request $request){
        $getIdProgram = DB::table('bkw_4_2_indikator_kegiatan_sub_bid')->where('id', $request->id)->first();
        $insert = DB::table('bkw_4_2_indikator_kegiatan_sub_bid')->where('id', $request->id)->update([
            'indikator' => $request->indikator,
            'satuan' => $request->satuan,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
                'id' => $getIdProgram->id_kegiatan_sub_bid
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }
}
