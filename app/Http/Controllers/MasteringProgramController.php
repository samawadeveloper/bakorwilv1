<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class MasteringProgramController extends Controller
{
    public function index(){
        $getSubBid = DB::table('bkw_master_sub_bidang')->get();
        $data = array(
            'subbid' => $getSubBid
        );
        return view('mastering_program.index')->with($data);
    }

    public function programAjax(){
        if(request()->ajax()) {
            $program = DB::table('bkw_1_1_program')
            ->leftJoin('bkw_1_2_indikator_program','bkw_1_1_program.id','=','bkw_1_2_indikator_program.id_program')
            ->select(DB::raw('bkw_1_1_program.*,bkw_1_2_indikator_program.id as id_indikator_program,bkw_1_2_indikator_program.indikator,bkw_1_2_indikator_program.satuan'))
            ->get();

            $programEach = array();
            $programTahunEach = array();
            $programTargetEach = array();
            $nop = 0;
            $nos = 0;

            foreach($program as $eachProgram){
                $getTarget = DB::table('bkw_1_3_target_indikator_program')->join('bkw_1_2_indikator_program','bkw_1_3_target_indikator_program.id_indikator_program','=','bkw_1_2_indikator_program.id')->select('bkw_1_3_target_indikator_program.tahun','bkw_1_3_target_indikator_program.target')->where('bkw_1_3_target_indikator_program.id_indikator_program', $eachProgram->id_indikator_program)->get();
                $programEach[$nop]['id'] = $eachProgram->id;
                $programEach[$nop]['id_indikator_program'] = $eachProgram->id_indikator_program;
                $programEach[$nop]['kode'] = $eachProgram->kode;
                $programEach[$nop]['program'] = $eachProgram->program;
                $programEach[$nop]['indikator'] = $eachProgram->indikator;
                $programEach[$nop]['satuan'] = $eachProgram->satuan;
                foreach($getTarget as $gtEach){
                    $programTahunEach[] = $gtEach->tahun;
                    $programTargetEach[] = $gtEach->target;

                    $nos++;
                }
                $programEach[$nop]['tahun'] = implode(",",$programTahunEach);
                $programEach[$nop]['target'] = implode(",",$programTargetEach);

                $nop++;
                $programTahunEach = array();
                $programTargetEach = array();
            }
            
            $dataKeg = array();
            $dataSubKeg = array();
            $dataSubBid = array();
            $noKeg = 0;
            $nono = 0;
            $noSub = 0;
            $noBid = 0;

            $kegiatanTahunEach = array();
            $kegiatanTargetEach = array();
            $nopk = 0;
            $nosk = 0;

            $kegiatanSubTahunEach = array();
            $kegiatanSubTargetEach = array();
            $nopksub = 0;
            $nosksub = 0;

            $kegiatanSubBidTahunEach = array();
            $kegiatanSubBidTargetEach = array();
            $nopksubbid = 0;
            $nosksubbid = 0;

            foreach($program as $eachProgram){
                $getKeg = DB::table('bkw_2_1_kegiatan')
                ->leftJoin('bkw_2_2_indikator_kegiatan','bkw_2_1_kegiatan.id','=','bkw_2_2_indikator_kegiatan.id_kegiatan')
                ->join('bkw_1_1_program','bkw_2_1_kegiatan.id_program','=','bkw_1_1_program.id')
                ->select('bkw_2_1_kegiatan.*','bkw_1_1_program.kode as kode_program','bkw_2_2_indikator_kegiatan.id as id_indikator_kegiatan','bkw_2_2_indikator_kegiatan.indikator','bkw_2_2_indikator_kegiatan.satuan')
                ->where('bkw_1_1_program.id', $eachProgram->id)
                ->orderBy('bkw_2_1_kegiatan.id', 'DESC')->get();
                
                    foreach($getKeg as $getKegEach){
                        $getTargetKeg = DB::table('bkw_2_3_target_indikator_kegiatan')->where('id_indikator_kegiatan', $getKegEach->id_indikator_kegiatan)->get();
                        
                        $getSubKeg = DB::table('bkw_3_1_kegiatan_sub')
                        ->leftJoin('bkw_3_2_indikator_kegiatan_sub','bkw_3_1_kegiatan_sub.id','=','bkw_3_2_indikator_kegiatan_sub.id_kegiatan_sub')
                        ->join('bkw_2_1_kegiatan','bkw_3_1_kegiatan_sub.id_kegiatan','=','bkw_2_1_kegiatan.id')
                        ->join('bkw_1_1_program','bkw_2_1_kegiatan.id_program','=','bkw_1_1_program.id')
                        ->select('bkw_3_1_kegiatan_sub.*','bkw_2_1_kegiatan.kode as kode_kegiatan','bkw_1_1_program.kode as kode_program','bkw_3_2_indikator_kegiatan_sub.id as id_indikator_sub_kegiatan','bkw_3_2_indikator_kegiatan_sub.indikator','bkw_3_2_indikator_kegiatan_sub.satuan')
                        ->where('bkw_3_1_kegiatan_sub.id_kegiatan', $getKegEach->id)
                        ->orderBy('bkw_3_1_kegiatan_sub.id', 'DESC')
                        ->get();
                        foreach($getSubKeg as $getSubKegEach){
                            $getTargetKegSub = DB::table('bkw_3_3_target_indikator_kegiatan_sub')->where('id_indikator_kegiatan_sub', $getSubKegEach->id_indikator_sub_kegiatan)->get();

                            $getSubBid = DB::table('bkw_4_1_kegiatan_sub_bid')
                            ->leftJoin('bkw_4_2_indikator_kegiatan_sub_bid','bkw_4_1_kegiatan_sub_bid.id','=','bkw_4_2_indikator_kegiatan_sub_bid.id_kegiatan_sub_bid')
                            ->join('bkw_3_1_kegiatan_sub','bkw_4_1_kegiatan_sub_bid.id_kegiatan_sub','=','bkw_3_1_kegiatan_sub.id')
                            ->join('bkw_2_1_kegiatan','bkw_3_1_kegiatan_sub.id_kegiatan','=','bkw_2_1_kegiatan.id')
                            ->join('bkw_1_1_program','bkw_2_1_kegiatan.id_program','=','bkw_1_1_program.id')
                            ->select('bkw_4_1_kegiatan_sub_bid.*','bkw_3_1_kegiatan_sub.id as id_sub_kegiatan','bkw_3_1_kegiatan_sub.kode as kode_sub_kegiatan','bkw_2_1_kegiatan.kode as kode_kegiatan','bkw_1_1_program.kode as kode_program','bkw_2_1_kegiatan.id as id_kegiatan','bkw_4_2_indikator_kegiatan_sub_bid.id as id_indikator_sub_bid_kegiatan','bkw_4_2_indikator_kegiatan_sub_bid.indikator','bkw_4_2_indikator_kegiatan_sub_bid.satuan')
                            ->where('bkw_4_1_kegiatan_sub_bid.id_kegiatan_sub', $getSubKegEach->id)
                            ->groupBy('bkw_4_1_kegiatan_sub_bid.id')
                            ->orderBy('bkw_4_1_kegiatan_sub_bid.id', 'DESC')
                            ->get();

                            foreach($getSubBid as $getSubBidEach){
                                $getTargetKegSubBid = DB::table('bkw_4_3_target_indikator_kegiatan_sub_bid')->where('id_indikator_kegiatan_sub_bid', $getSubBidEach->id_indikator_sub_bid_kegiatan)->get();

                                foreach($getTargetKegSubBid as $gtKegSubBidEach){
                                    $kegiatanSubBidTahunEach[] = $gtKegSubBidEach->tahun;
                                    $kegiatanSubBidTargetEach[] = $gtKegSubBidEach->target;
                
                                    $nosksubbid++;
                                }
                                $dataSubBid[$noBid]['tahun'] = implode(",",$kegiatanSubBidTahunEach);
                                $dataSubBid[$noBid]['target'] = implode(",",$kegiatanSubBidTargetEach);
                
                                $nopksubbid++;
                                $kegiatanSubBidTahunEach = array();
                                $kegiatanSubBidTargetEach = array();


                                $dataSubBid[$noBid]['id_sub_bid'] = $getSubBidEach->id;
                                $dataSubBid[$noBid]['id_sub_kegiatan'] = $getSubBidEach->id_sub_kegiatan;
                                $dataSubBid[$noBid]['id_indikator_sub_bid_kegiatan'] = $getSubBidEach->id_indikator_sub_bid_kegiatan;
                                $dataSubBid[$noBid]['id_kegiatan'] = $getSubBidEach->id_kegiatan;
                                $dataSubBid[$noBid]['kode'] = $getSubBidEach->kode;
                                $dataSubBid[$noBid]['kode_kegiatan'] = $getSubBidEach->kode_kegiatan;
                                $dataSubBid[$noBid]['kode_sub_kegiatan'] = $getSubBidEach->kode_sub_kegiatan;
                                $dataSubBid[$noBid]['sub_bid_kegiatan'] = $getSubBidEach->sub_bid_kegiatan;
                                $dataSubBid[$noBid]['kode_program'] = $getSubBidEach->kode_program;
                                $dataSubBid[$noBid]['indikator'] = $getSubBidEach->indikator;
                                $dataSubBid[$noBid]['satuan'] = $getSubBidEach->satuan;
                                $dataSubBid[$noBid]['pagu'] = $getSubBidEach->pagu;
                                $noBid++;
                            }

                            foreach($getTargetKegSub as $gtKegSubEach){
                                $kegiatanSubTahunEach[] = $gtKegSubEach->tahun;
                                $kegiatanSubTargetEach[] = $gtKegSubEach->target;
            
                                $nosksub++;
                            }
                            $dataSubKeg[$noSub]['tahun'] = implode(",",$kegiatanSubTahunEach);
                            $dataSubKeg[$noSub]['target'] = implode(",",$kegiatanSubTargetEach);
            
                            $nopksub++;
                            $kegiatanSubTahunEach = array();
                            $kegiatanSubTargetEach = array();

                            $dataSubKeg[$noSub]['id_sub_kegiatan'] = $getSubKegEach->id;
                            $dataSubKeg[$noSub]['id_indikator_sub_kegiatan'] = $getSubKegEach->id_indikator_sub_kegiatan;
                            $dataSubKeg[$noSub]['id_kegiatan'] = $getSubKegEach->id_kegiatan;
                            $dataSubKeg[$noSub]['kode'] = $getSubKegEach->kode;
                            $dataSubKeg[$noSub]['kode_kegiatan'] = $getSubKegEach->kode_kegiatan;
                            $dataSubKeg[$noSub]['sub_kegiatan'] = $getSubKegEach->sub_kegiatan;
                            $dataSubKeg[$noSub]['kode_program'] = $getSubKegEach->kode_program;
                            $dataSubKeg[$noSub]['indikator'] = $getSubKegEach->indikator;
                            $dataSubKeg[$noSub]['satuan'] = $getSubKegEach->satuan;
                            $noSub++;
                        }

                        foreach($getTargetKeg as $gtKegEach){
                            $kegiatanTahunEach[] = $gtKegEach->tahun;
                            $kegiatanTargetEach[] = $gtKegEach->target;
        
                            $nosk++;
                        }
                        $dataKeg[$nono]['tahun'] = implode(",",$kegiatanTahunEach);
                        $dataKeg[$nono]['target'] = implode(",",$kegiatanTargetEach);
        
                        $kegiatanTahunEach = array();
                        $kegiatanTargetEach = array();

                        $dataKeg[$nono]['id_kegiatan'] = $getKegEach->id;
                        $dataKeg[$nono]['id_indikator_kegiatan'] = $getKegEach->id_indikator_kegiatan;
                        $dataKeg[$nono]['kode'] = $getKegEach->kode;
                        $dataKeg[$nono]['indikator'] = $getKegEach->indikator;
                        $dataKeg[$nono]['satuan'] = $getKegEach->satuan;
                        $dataKeg[$nono]['kegiatan'] = $getKegEach->kegiatan;
                        $dataKeg[$nono]['id_program'] = $getKegEach->id_program;
                        $dataKeg[$nono]['kode_program'] = $getKegEach->kode_program;

                        $nono++;
                    }
                
                
                $noKeg++;
            }

            if($program){
                return response()->json([
                    'error' => 'false',
                    'message' => 'Berhasil',
                    'data' => $programEach,
                    'kegiatan' => $dataKeg,
                    'sub_kegiatan' => $dataSubKeg,
                    'sub_bidang' => array_unique($dataSubBid, SORT_REGULAR),
                ], 200);
            }else{
                return response()->json('Gagal', 401);
            }
        }
    }

    public function prosesTambah(Request $request){
        $kode = $request->kode;
        $program = $request->program;

        $insert = DB::table('bkw_1_1_program')->insert([
            'kode' => $kode,
            'program' => $program,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD PROGRAM',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan PROGRAM ' . $request->program,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }
    public function prosesTambahSubKegiatan(Request $request){
        $kode = $request->kode;
        $sub_kegiatan = $request->sub_kegiatan;
        $id_kegiatan = $request->id_kegiatan;

        $insert = DB::table('bkw_3_1_kegiatan_sub')->insert([
            'kode' => $kode,
            'sub_kegiatan' => $sub_kegiatan,
            'id_kegiatan' => $id_kegiatan,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD SUB KEGIATAN',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan SUB KEGIATAN ' . $request->sub_kegiatan,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }
    public function prosesTambahSubBidangKegiatan(Request $request){
        $kode = $request->kode;
        $sub_sub_kegiatan = $request->sub_sub_kegiatan;
        $id_sub_kegiatan = $request->id_sub_kegiatan;
        $id_sub_bidang = $request->id_sub_bidang;

        $insert = DB::table('bkw_4_1_kegiatan_sub_bid')->insert([
            'kode' => $kode,
            'sub_bid_kegiatan' => $sub_sub_kegiatan,
            'id_kegiatan_sub' => $id_sub_kegiatan,
            'id_sub_bidang' => $id_sub_bidang,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD SUB BIDANG KEGIATAN',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan SUB BIDANG KEGIATAN ' . $request->sub_sub_kegiatan,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }
    public function prosesTambahKegiatan(Request $request){
        $kode = $request->kode;
        $kegiatan = $request->kegiatan;
        $id_program = $request->id_program;

        $insert = DB::table('bkw_2_1_kegiatan')->insert([
            'kode' => $kode,
            'kegiatan' => $kegiatan,
            'id_program' => $id_program,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD KEGIATAN',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan KEGIATAN ' . $request->kegiatan,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }
    public function prosesTambahIndikatirProgram(Request $request){
        $id_program_indikator = $request->id_program_indikator;
        $indikator_program = $request->indikator_program;
        $satuan_program = $request->satuan_program;
        
        $insert = DB::table('bkw_1_2_indikator_program')->insertGetId([
            'id_program' => $id_program_indikator,
            'indikator' => $indikator_program,
            'satuan' => $satuan_program,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);
        
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD INDIKATOR PROGRAM',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan INDIKATOR PROGRAM ' . $indikator_program,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }

    public function prosesTambahIndikatirKegiatan(Request $request){
        $id_kegiatan_indikator = $request->id_kegiatan_indikator;
        $indikator_kegiatan = $request->indikator_kegiatan;
        $satuan_kegiatan = $request->satuan_kegiatan;
        
        $insert = DB::table('bkw_2_2_indikator_kegiatan')->insertGetId([
            'id_kegiatan' => $id_kegiatan_indikator,
            'indikator' => $indikator_kegiatan,
            'satuan' => $satuan_kegiatan,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);
        
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD INDIKATOR KEGIATAN',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan INDIKATOR KEGIATAN ' . $indikator_kegiatan,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }
    public function prosesTambahIndikatirSubKegiatan(Request $request){
        $id_sub_kegiatan_indikator = $request->id_sub_kegiatan_indikator;
        $indikator_sub_kegiatan = $request->indikator_sub_kegiatan;
        $satuan_sub_kegiatan = $request->satuan_sub_kegiatan;
        
        $insert = DB::table('bkw_3_2_indikator_kegiatan_sub')->insertGetId([
            'id_kegiatan_sub' => $id_sub_kegiatan_indikator,
            'indikator' => $indikator_sub_kegiatan,
            'satuan' => $satuan_sub_kegiatan,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);
        
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD INDIKATOR SUB KEGIATAN',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan INDIKATOR SUB KEGIATAN ' . $indikator_sub_kegiatan,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }
    public function prosesTambahIndikatirSubBid(Request $request){
        $id_sub_bidang_indikator = $request->id_sub_bidang_indikator;
        $indikator_sub_bidang = $request->indikator_sub_bidang;
        $satuan_sub_bidang = $request->satuan_sub_bidang;
        
        $insert = DB::table('bkw_4_2_indikator_kegiatan_sub_bid')->insertGetId([
            'id_kegiatan_sub_bid' => $id_sub_bidang_indikator,
            'indikator' => $indikator_sub_bidang,
            'satuan' => $satuan_sub_bidang,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);
        
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD INDIKATOR SUB BIDANG',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan INDIKATOR SUB BIDANG ' . $indikator_sub_bidang,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }

    public function prosesTambahTargetProgram(Request $request){
        $insert = DB::table('bkw_1_3_target_indikator_program')->insertGetId([
            'id_indikator_program' => $request->id_indikator,
            'tahun' => $request->tahun,
            'target' => $request->target,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD TARGET PROGRAM',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan TARGET PROGRAM ' . $request->tahun. ' '. $request->target,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);
        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }
    public function prosesTambahTargetKegiatan(Request $request){
        $insert = DB::table('bkw_2_3_target_indikator_kegiatan')->insertGetId([
            'id_indikator_kegiatan' => $request->id_indikator,
            'tahun' => $request->tahun,
            'target' => $request->target,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD TARGET KEGIATAN',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan TARGET KEGIATAN ' . $request->tahun. ' '. $request->target,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);
        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }
    public function prosesTambahTargetSubKegiatan(Request $request){
        $insert = DB::table('bkw_3_3_target_indikator_kegiatan_sub')->insertGetId([
            'id_indikator_kegiatan_sub' => $request->id_indikator,
            'tahun' => $request->tahun,
            'target' => $request->target,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD TARGET SUB KEGIATAN',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan TARGET SUB KEGIATAN ' . $request->tahun. ' '. $request->target,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);
        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }
    public function prosesTambahTargetSubBidang(Request $request){
        $insert = DB::table('bkw_4_3_target_indikator_kegiatan_sub_bid')->insertGetId([
            'id_indikator_kegiatan_sub_bid' => $request->id_indikator,
            'tahun' => $request->tahun,
            'target' => $request->target,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD TARGET SUB BIDANG',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan TARGET SUB BIDANG ' . $request->tahun. ' '. $request->target,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);
        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }

    public function prosesAturPagu(Request $request){
        $id_sub_bidang = $request->id_sub_bidang;
        $pagu = $request->pagu;

        $update = DB::table('bkw_4_1_kegiatan_sub_bid')->where('id', $id_sub_bidang)->update([
            'pagu' => $pagu
        ]);
        if($update){
            return response()->json([
                'error' => 'false',
                'message' => 'Update Berhasil',
            ], 200);
        }else{
            return response()->json('Update Gagal', 401);
        }


    }


}
