<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class SubBidangController extends Controller
{
    public function index(){
        if(request()->ajax()) {
            $subid = DB::table('bkw_master_sub_bidang')->get();
            return Datatables::of($subid)
                ->addColumn('action', function ($subid) {
                    return '<a href="/ubah-sub-bidang/'.$subid->id.'" class="btn btn-info btn-sm">Ubah</a>';
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('sub_bidang.index');
    }

    public function tambah(){
        return view('sub_bidang.tambah');
    }

    public function prosesTambah(Request $request){
        
        $insert = DB::table('bkw_master_sub_bidang')->insertGetId([
            'sub_bid' => $request->sub_bid,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        //Catat Log
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD SUB BIDANG',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan SUB BIDANG ' . $request->sub_bid,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }

    public function prosesUbah(Request $request){
        $update = DB::table('bkw_master_sub_bidang')->where('id', $request->id)->update([
            'sub_bid' => $request->sub_bid,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => Session::get('sessionIdAdmin'),
        ]);

        //Catat Log
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'UBAH SUB BIDANG',
            'deskripsi' => 'Melakukan Aktifitas Mengubah SUB BIDANG ' . $request->sub_bid,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($update){
            return response()->json([
                'error' => 'false',
                'message' => 'Update Berhasil',
            ], 200);
        }else{
            return response()->json('Update Gagal', 401);
        }
    }

    public function ubah($id){
        $getData = DB::table('bkw_master_sub_bidang')->where('id', $id)->first();
        $data = array(
            'data' => $getData,
        );

        return view('sub_bidang.ubah')->with($data);
    }
}
