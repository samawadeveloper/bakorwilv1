<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class MasterProgramController extends Controller
{
    public function index(){
        if(request()->ajax()) {
            $program = DB::table('bkw_1_1_program')->get();
            return Datatables::of($program)
                ->addColumn('action', function ($program) {
                    return '<a href="/ubah-program/'.$program->id.'" class="btn btn-sm btn-outline-primary">Ubah</a> <a href="/indikator-program/'.$program->id.'" class="btn btn-sm btn-outline-success">Indikator</a>';
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('master.program.index');
    }

    public function indikator($id){
        if(request()->ajax()) {
            $program = DB::table('bkw_1_2_indikator_program')->where('id_program', $id)->get();
            return Datatables::of($program)
                ->addColumn('action', function ($program) {
                    return '<a href="/ubah-indikator-program/'.$program->id.'" class="btn btn-sm btn-outline-primary">Ubah</a> <a href="/target-program/'.$program->id.'" class="btn btn-sm btn-outline-danger">Target</a>';
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        $getNameProgram = DB::table('bkw_1_1_program')->where('id', $id)->first();
        $data = array(
            'id' => $id,
            'program' => $getNameProgram->program,
            'kode' => $getNameProgram->kode,
        );
        return view('master.program.indikator')->with($data);
    }

    public function tambah(){
        return view('master.program.tambah');
    }
    public function tambahIndikator($id){
        
        $data=array(
            'id' => $id,
            
        );
        return view('master.program.tambahIndikator')->with($data);
    }
    public function ubahIndikator($id){
        $d = DB::table('bkw_1_2_indikator_program')->where('id', $id)->first();
        $data=array(
            'id' => $id,
            'd' => $d,
            
        );
        return view('master.program.ubahIndikator')->with($data);
    }

    public function prosesTambahIndikator(Request $request){
        $insert = DB::table('bkw_1_2_indikator_program')->insert([
            'id_program' => $request->id,
            'indikator' => $request->indikator,
            'satuan' => $request->satuan,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }
    public function prosesUbahIndikator(Request $request){
        $getIdProgram = DB::table('bkw_1_2_indikator_program')->where('id', $request->id)->first();
        $insert = DB::table('bkw_1_2_indikator_program')->where('id', $request->id)->update([
            'indikator' => $request->indikator,
            'satuan' => $request->satuan,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
                'id' => $getIdProgram->id_program
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }

    public function prosesTambah(Request $request){
        $insert = DB::table('bkw_1_1_program')->insert([
            'kode' => $request->kode,
            'program' => $request->program,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }
    
    public function ubah($id){
        $get = DB::table('bkw_1_1_program')->where('id', $id)->first();
        $data = array(
            'data' => $get
        );

        return view('master.program.ubah')->with($data);
    }
    
    public function prosesUbah(Request $request){
        $update = DB::table('bkw_1_1_program')->where('id', $request->id)->update([
            'kode' => $request->kode,
            'program' => $request->program,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => Session::get('sessionIdAdmin'),
        ]);

        if($update){
            return response()->json([
                'error' => 'false',
                'message' => 'Success',
            ], 200);
        }else{
            return response()->json('Failed', 401);
        }
    }
}
