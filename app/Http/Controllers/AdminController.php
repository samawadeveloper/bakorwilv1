<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function dashboard(){

        if((Session::get('sessionIdAdmin') != "") && (Session::get('sessionEmailAdmin') != "") && (Session::get('sessionTokenAdmin') != "") && (Session::get('sessionNamaAdmin') != "") ){
            $userData = DB::table('bkw_master_user')
                ->where('username',Session::get('sessionEmailAdmin'))
                ->where('id',Session::get('sessionIdAdmin'))->first();

            $data = array(
                'data_admin' => $userData,
            );


            // Menampilkan data dashboard dengan role: superadmin
            switch (Session::get('sessionRoleAdmin')) {
                case 1:
                    // if `superadmin`
                    $dSuperAdmin = $this->getDashboardSuperadmin($userData,Session::get('sessionIdAdmin'),Session::get('sessionEmailAdmin'));
                    return view('dashboard_sa')->with('cData',$dSuperAdmin);
                    break;

                case 2:
                    $dAdmin = $this->getDashboardAdmin($userData);
                    return view('dashboard_adm')->with('cData',$dAdmin);
                    break;

                default:
                    return view('dashboard')->with($data);
                    break;
            }

        }else{
            return redirect('/');
        }
    }

    /* Dashboard Superadmin */
    private function getDashboardSuperadmin($userdata,$sessionId=null,$sessionUser=null){

        // Menampilkan Total item master program, kegiatan, subkegiatan, sub-sub kegiatan
        $cProgram = DB::table('bkw_1_1_program')->count();
        $cKegiatan = DB::table('bkw_2_1_kegiatan')->count();
        $cSubKegiatan = DB::table('bkw_3_1_kegiatan_sub')->count();
        $cSubSubKegiatan = DB::table('bkw_4_1_kegiatan_sub_bid')->count();

        // List User Log
        $cActivityList = DB::table('bkw_t_log')
                            ->select('bkw_t_log.*','bkw_master_user.nama')
                            ->join('bkw_master_user', 'bkw_master_user.id','=','bkw_t_log.log_by')
                            ->get();

        // Parsing data ke satu array
        $data = array(
            'uData' => $userdata,
            'uMasterProgram' => array(
                    'cProg' => $cProgram,
                    'cKeg' => $cKegiatan,
                    'cSubKeg' => $cSubKegiatan,
                    'cSubSubKeg' => $cSubSubKegiatan,
            ),
            'uListActivity' => $cActivityList,
        );
        return $data;
    }

    /* Dashboard Admin */
    private function getDashboardAdmin($userdata){
        // Menampilkan Total item master program, kegiatan, subkegiatan, sub-sub kegiatan
        $cProgram = DB::table('bkw_1_1_program')->count();
        $cKegiatan = DB::table('bkw_2_1_kegiatan')->count();
        $cSubKegiatan = DB::table('bkw_3_1_kegiatan_sub')->count();
        $cSubSubKegiatan = DB::table('bkw_4_1_kegiatan_sub_bid')->count();

        // List User Log
        $cActivityList = DB::table('bkw_t_log')
            ->select('bkw_t_log.*', 'bkw_master_user.nama')
            ->join('bkw_master_user', 'bkw_master_user.id', '=', 'bkw_t_log.log_by')
            ->get();

        // Parsing data ke satu array
        $data = array(
            'uData' => $userdata,
            'uMasterProgram' => array(
                'cProg' => $cProgram,
                'cKeg' => $cKegiatan,
                'cSubKeg' => $cSubKegiatan,
                'cSubSubKeg' => $cSubSubKegiatan,
            ),
            'uListActivity' => $cActivityList,
        );
        return $data;
    }

    /* Dashboard Kepala Bakorwil */
    /* private function getDashboardKepalaBakorwil(){

    } */

    /* Dashboard Kabid */
    /* private function getDashboardKabid(){

    } */



    public function sasaran(){
        return view('wizardLaporanKinerjbkw_t_logwStep1Sasaran');
    }
    public function kegiatan(){
        return view('wizardLaporanKinerjbkw_t_logwStep2Kegiatan');
    }
    public function subKegiatan(){
        return view('wizardLaporanKinerjbkw_t_logwStep3subKegiatan');
    }
    public function pembagianKegiatan(){
        return view('wizardLaporanKinerjbkw_t_logwStep4pembagianKegiatan');
    }

}
