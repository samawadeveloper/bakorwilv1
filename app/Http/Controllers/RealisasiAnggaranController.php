<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class RealisasiAnggaranController extends Controller
{
    public function index(){
        if(request()->ajax()) {
            
            $get = DB::table('bkw_4_1_kegiatan_sub_bid')
                ->leftJoin('bkw_t_realisasi_anggaran','bkw_4_1_kegiatan_sub_bid.id','bkw_t_realisasi_anggaran.id_kegiatan_sub_bid')
                ->join('bkw_3_1_kegiatan_sub','bkw_4_1_kegiatan_sub_bid.id_kegiatan_sub','bkw_3_1_kegiatan_sub.id')
                ->join('bkw_master_sub_bidang','bkw_4_1_kegiatan_sub_bid.id_sub_bidang','bkw_master_sub_bidang.id')
                ->join('bkw_2_1_kegiatan','bkw_3_1_kegiatan_sub.id_kegiatan','bkw_2_1_kegiatan.id')
                ->join('bkw_1_1_program','bkw_2_1_kegiatan.id_program','bkw_1_1_program.id')
                ->select('bkw_t_realisasi_anggaran.id as id_realisasi_anggaran','bkw_t_realisasi_anggaran.keterangan','bkw_t_realisasi_anggaran.realisasi_anggaran','bkw_4_1_kegiatan_sub_bid.id','bkw_1_1_program.kode as kode_program','bkw_1_1_program.program','bkw_2_1_kegiatan.kode as kode_kegiatan','bkw_2_1_kegiatan.kegiatan','bkw_3_1_kegiatan_sub.kode as kode_sub_kegiatan','bkw_3_1_kegiatan_sub.sub_kegiatan','bkw_4_1_kegiatan_sub_bid.kode as kode_sub_kegiatan_bidang','bkw_4_1_kegiatan_sub_bid.sub_bid_kegiatan','bkw_master_sub_bidang.sub_bid','bkw_4_1_kegiatan_sub_bid.pagu')
                ->get();
                return Datatables::of($get)
                ->addColumn('action', function ($get) {
                    if($get->realisasi_anggaran == ""){
                        return '<a href="javascript:void(0)" onclick="dataModalRealisasi('.$get->id.','.$get->pagu.')" data-bs-toggle="modal" data-bs-target="#realisasi"><svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><rect x="4" y="4" width="16" height="16" rx="2" /><line x1="9" y1="12" x2="15" y2="12" /><line x1="12" y1="9" x2="12" y2="15" /></svg></a>';
                    }else{
                        $prosen = ($get->realisasi_anggaran / $get->pagu) * 100;
                        return '<a href="javascript:void(0)" onclick="dataModalRealisasiUpdate('.$get->id.','.$get->id_realisasi_anggaran.','.$get->pagu.','.$get->realisasi_anggaran.',\'' . $get->keterangan . '\')" data-bs-toggle="modal" data-bs-target="#realisasi-update">'.number_format($get->realisasi_anggaran, 0, ",", ".").'</a> <b>('.$prosen.'%)</b>';
                    }
                    
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('realisasi_anggaran.index');
    }

    public function prosesRealisasi(Request $request){
        $id = $request->id_kegiatan_sub_bidang;
        $realisasi = $request->realisasi;

        $insert = DB::table('bkw_t_realisasi_anggaran')->insert([
            'id_kegiatan_sub_bid' => $id,
            'realisasi_anggaran' => $realisasi,
            'keterangan' => $request->keterangan,
            'input_at' => date('Y-m-d H:i:s'),
            'input_by' => Session::get('sessionIdAdmin'),
        ]);

        //Catat Log
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'ADD REALISASI',
            'deskripsi' => 'Melakukan Aktifitas Menambahkan REALISASI ' . $realisasi.'-'.$id,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Insert Berhasil',
            ], 200);
        }else{
            return response()->json('Insert Gagal', 401);
        }
    }
    public function prosesRealisasiUbah(Request $request){
        $id = $request->id_realisasi_anggaran;
        $realisasi = $request->realisasi;

        $insert = DB::table('bkw_t_realisasi_anggaran')->where('id', $id)->update([
            'realisasi_anggaran' => $realisasi,
            'keterangan' => $request->keterangan,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => Session::get('sessionIdAdmin'),
        ]);

        //Catat Log
        DB::table('bkw_t_log')->insert([
            'nama_log' => 'UBAH REALISASI',
            'deskripsi' => 'Melakukan Aktifitas Mengubah REALISASI ' . $realisasi.'-'.$id,
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        if($insert){
            return response()->json([
                'error' => 'false',
                'message' => 'Update Berhasil',
            ], 200);
        }else{
            return response()->json('Update Gagal', 401);
        }
    }
}
