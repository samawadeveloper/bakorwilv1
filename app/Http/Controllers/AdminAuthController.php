<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AdminAuthController extends Controller
{
    public function loginPage(){
        return view('auth.login');
    }
    public function authLogin(Request $request){
        if(request()->ajax()) {
            $username = $request->input('username');
            $password = md5($request->input('password'));
            if ((isset($username)) || (isset($password))) {

                $cekUsername = DB::table('bkw_master_user')
                    ->where('username', $username)
                    ->count();

                if ($cekUsername > 0) {
                    $cekUserActive = DB::table('bkw_master_user')
                        ->where('username', $username)
                        ->where('password', $password)
                        ->count();

                    if ($cekUserActive > 0) {

                        $cekActive = DB::table('bkw_master_user')
                        ->where('username', $username)
                        ->where('password', $password)
                        ->where('aktif', 1)
                        ->count();

                        if ($cekActive > 0) {

                            //Set Token
                            $last_login = date('Y-m-d H:i:s');
                            $api_token = $this->str_random(60);
                            $setToken = DB::table('bkw_master_user')->where('username', $username)->update([
                                'api_token_web' => $api_token,
                                'last_login' => $last_login,
                            ]);

                            if($setToken){
                                $getData = DB::table('bkw_master_user')
                                    ->where('username', $username)
                                    ->leftJoin('bkw_master_sub_bidang','bkw_master_user.id_posisi','=','bkw_master_sub_bidang.id')
                                    ->select('bkw_master_user.id','bkw_master_user.username','bkw_master_user.nama','bkw_master_user.api_token_web','bkw_master_user.status','bkw_master_user.aktif','bkw_master_sub_bidang.sub_bid')
                                    ->where('password', $password)
                                    ->first();
                                //Set Session
                                $this->setSessionGoogleAuth($getData->id, $getData->username, $getData->api_token_web, $getData->nama, $getData->status);

                                //Catat Log
                                DB::table('bkw_t_log')->insert([
                                    'nama_log' => 'LOGIN',
                                    'deskripsi' => 'Melakukan Aktifitas Login Dengan Username ' . $username,
                                    'log_at' => date('Y-m-d H:i:s'),
                                    'log_by' => $getData->id,
                                ]);


                                return response()->json([
                                    'error' => 'false',
                                    'message' => 'Berhasil Login!',
                                ], 200);
                            }else{
                                return response()->json('Gagal Set Token!', 401);
                            }
                        }else{
                            return response()->json('Akun di nonaktifkan!', 401);
                        }

                    } else {
                        return response()->json('Password Tidak Sesuai Dengan Username!', 401);

                    }
                } else {
                    return response()->json('Username Tidak Terdaftar!',401);
                }

            }else{
                return response()->json('Username / Password Kosong!', 401);
            }
        }else{
            echo "No Ajax Request";
        }
    }

    public function setSessionGoogleAuth($id, $email, $token, $nama, $role){
        session([
            'sessionRoleAdmin' => $role ,
            'sessionIdAdmin' => $id ,
            'sessionEmailAdmin' => $email ,
            'sessionTokenAdmin' => $token,
            'sessionNamaAdmin' => $nama
        ]);
    }

    function str_random($length = 16)
    {
        return Str::random($length);
    }

    public function authLogout(){

        DB::table('bkw_t_log')->insert([
            'nama_log' => 'LOGIN',
            'deskripsi' => 'Melakukan Aktifitas Logout Dengan Username ' . Session::get('sessionEmailAdmin'),
            'log_at' => date('Y-m-d H:i:s'),
            'log_by' => Session::get('sessionIdAdmin'),
        ]);

        Session::forget('sessionRoleAdmin');
        Session::forget('sessionIdAdmin');
        Session::forget('sessionEmailAdmin');
        Session::forget('sessionTokenAdmin');
        Session::forget('sessionNamaAdmin');
        Session::flush();

        return redirect('/');
    }
}
