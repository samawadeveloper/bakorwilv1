<?php

namespace App\Http\Middleware;

use Closure;

class CheckAksesAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->session()->exists('sessionIdAdmin')) {
            return redirect('/');
        }
        return $next($request);
    }
}
