<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\DB;

use Closure;

class CheckAuthAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->exists('sessionIdAdmin')) {
            $id = $request->session()->get('sessionIdAdmin');
            // user value cannot be found in session
            $getToken = DB::table('bkw_master_user')
                ->select('api_token_web')
                ->where('id', $id)->first();

            if($request->session()->get('sessionTokenAdmin') == $getToken->api_token_web){
                return redirect('/dashboard');
            }
        }
        return $next($request);
    }
}
