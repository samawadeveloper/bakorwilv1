<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class Menu
{

    public static function getMenusRole($cRole)
    {
        $cMenu = DB::table('user_menus')
            ->where('group_id',$cRole)
            ->where('is_active',1)
            ->orderBy('position','asc')
            ->get();

        $cat = array(
            'items' => array(),
            'parents' => array()
        );

        // build array list with menu data
        foreach ($cMenu as $cats) {
            // menampung items array
            $cat['items'][$cats->menu_id] = $cats;
            // menampung parent array
            $cat['parents'][$cats->parent_id][] = $cats->menu_id;
        }

        if($cat){
            $cBuild = self::buildMenu(0,$cat);
            // $cBuild = $cat;
            return $cBuild;
        }else{
            return false;
        }
    }

    private static function buildMenu($parent,$menu)
    {
        $cHtml = "";
        if (isset($menu['parents'][$parent])) {

            foreach ($menu['parents'][$parent] as $itemId) {

                if (!isset($menu['parents'][$itemId])) {
                    // $cHtml .= '<a class="dropdown-item" href="' . $menu['items'][$itemId]->url . '">';
                    $cHtml .= '<a class="nav-link" href="' . $menu['items'][$itemId]->url . '" style="justify-content:normal;">';
                    $cHtml .= '<span class="nav-link-icon d-md-none d-lg-inline-block">';
                    $cHtml .= $menu['items'][$itemId]->icon;
                    $cHtml .= '</span>';
                    $cHtml .= '<span class="nav-link-title">';
                    $cHtml .= $menu['items'][$itemId]->title;
                    $cHtml .= '</span>';
                    $cHtml .= '</a>';
                }

                if (isset($menu['parents'][$itemId])) {

                    $cHtml .= '<li class="nav-item dropdown">';
                    $cHtml .= '<a class="nav-link dropdown-toggle" href="'.$menu['items'][$itemId]->url. '" data-bs-toggle="dropdown" role="button" aria-expanded="false" >';
                    $cHtml .= '<span class="nav-link-icon d-md-none d-lg-inline-block">';
                    $cHtml .= $menu['items'][$itemId]->icon;
                    $cHtml .= '</span>';
                    $cHtml .= '<span class="nav-link-title">';
                    $cHtml .= $menu['items'][$itemId]->title;
                    $cHtml .= '</span>';
                    $cHtml .= '</a>';
                    $cHtml .= '<div class="dropdown-menu">';
                    $cHtml .= self::buildMenu($itemId,$menu);
                    $cHtml .= '</div>';
                    $cHtml .= '</li>';
                }
            }
        }
        return $cHtml;
    }

    public static function get_username($user_id)
    {
        $user = DB::table('users')->where('userid', $user_id)->first();
        return (isset($user->username) ? $user->username : '');
    }
}
