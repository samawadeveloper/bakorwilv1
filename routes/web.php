<?php
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::get('/cc', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});


Route::group(['middleware' => 'authAdmin'], function () {
    Route::get('/', 'AdminAuthController@loginPage');
    Route::post('/auth-login-admin', 'AdminAuthController@authLogin');
});

Route::group(['middleware' => 'aksesAdmin'], function () {
    Route::get('/dashboard', 'AdminController@dashboard')->name('dashboard');
    Route::get('/laporan-kinerja-baru/sasaran', 'AdminController@sasaran');
    Route::get('/laporan-kinerja-baru/kegiatan', 'AdminController@kegiatan');
    Route::get('/laporan-kinerja-baru/sub-kegiatan', 'AdminController@subKegiatan');
    Route::get('/laporan-kinerja-baru/pembagian-kegiatan', 'AdminController@pembagianKegiatan');
    Route::get('/logout', 'AdminAuthController@authLogout');

    //Master Program
    Route::get('/program', 'MasterProgramController@index');
    Route::get('/tambah-program', 'MasterProgramController@tambah');
    Route::post('/proses-tambah-program', 'MasterProgramController@prosesTambah');
    Route::get('/ubah-program/{id}', 'MasterProgramController@ubah');
    Route::post('/proses-ubah-program', 'MasterProgramController@prosesUbah');

    Route::get('/indikator-program/{program}', 'MasterProgramController@indikator');
    Route::get('/tambah-indikator-program/{program}', 'MasterProgramController@tambahIndikator');
    Route::post('/proses-tambah-indikator-program', 'MasterProgramController@prosesTambahIndikator');
    Route::get('/ubah-indikator-program/{indikator}', 'MasterProgramController@ubahIndikator');
    Route::post('/proses-ubah-indikator-program', 'MasterProgramController@prosesUbahIndikator');
    
    //Master Kegiatan
    Route::get('/kegiatan', 'MasterKegiatanController@index');
    Route::get('/tambah-kegiatan', 'MasterKegiatanController@tambah');
    Route::post('/proses-tambah-kegiatan', 'MasterKegiatanController@prosesTambah');
    Route::get('/ubah-kegiatan/{id}', 'MasterKegiatanController@ubah');
    Route::post('/proses-ubah-kegiatan', 'MasterKegiatanController@prosesUbah');

    Route::get('/indikator-kegiatan/{kegiatan}', 'MasterKegiatanController@indikator');
    Route::get('/tambah-indikator-kegiatan/{kegiatan}', 'MasterKegiatanController@tambahIndikator');
    Route::post('/proses-tambah-indikator-kegiatan', 'MasterKegiatanController@prosesTambahIndikator');
    Route::get('/ubah-indikator-kegiatan/{kegiatan}', 'MasterKegiatanController@ubahIndikator');
    Route::post('/proses-ubah-indikator-kegiatan', 'MasterKegiatanController@prosesUbahIndikator');
    
    //Master Sub Kegiatan
    Route::get('/sub-kegiatan', 'MasterSubKegiatanController@index');
    Route::get('/get-kegiatan/{id}', 'MasterSubKegiatanController@getKegiatan');
    Route::get('/tambah-sub-kegiatan', 'MasterSubKegiatanController@tambah');
    Route::post('/proses-tambah-sub-kegiatan', 'MasterSubKegiatanController@prosesTambah');
    Route::get('/ubah-sub-kegiatan/{id}', 'MasterSubKegiatanController@ubah');
    Route::post('/proses-ubah-sub-kegiatan', 'MasterSubKegiatanController@prosesUbah');
    
    Route::get('/indikator-sub-kegiatan/{kegiatan}', 'MasterSubKegiatanController@indikator');
    Route::get('/tambah-indikator-sub-kegiatan/{kegiatan}', 'MasterSubKegiatanController@tambahIndikator');
    Route::post('/proses-tambah-indikator-sub-kegiatan', 'MasterSubKegiatanController@prosesTambahIndikator');
    Route::get('/ubah-indikator-sub-kegiatan/{kegiatan}', 'MasterSubKegiatanController@ubahIndikator');
    Route::post('/proses-ubah-indikator-sub-kegiatan', 'MasterSubKegiatanController@prosesUbahIndikator');

    //Master Sub Sub Kegiatan
    Route::get('/sub-sub-kegiatan', 'MasterSubSubKegiatanController@index');
    Route::get('/get-sub-kegiatan/{id}', 'MasterSubSubKegiatanController@getSubKegiatan');
    Route::get('/tambah-sub-sub-kegiatan', 'MasterSubSubKegiatanController@tambah');
    Route::post('/proses-tambah-sub-sub-kegiatan', 'MasterSubSubKegiatanController@prosesTambah');
    Route::get('/ubah-sub-sub-kegiatan/{id}', 'MasterSubSubKegiatanController@ubah');
    Route::post('/proses-ubah-sub-sub-kegiatan', 'MasterSubSubKegiatanController@prosesUbah');

    Route::get('/indikator-sub-sub-kegiatan/{kegiatan}', 'MasterSubSubKegiatanController@indikator');
    Route::get('/tambah-indikator-sub-sub-kegiatan/{kegiatan}', 'MasterSubSubKegiatanController@tambahIndikator');
    Route::post('/proses-tambah-indikator-sub-sub-kegiatan', 'MasterSubSubKegiatanController@prosesTambahIndikator');
    Route::get('/ubah-indikator-sub-sub-kegiatan/{kegiatan}', 'MasterSubSubKegiatanController@ubahIndikator');
    Route::post('/proses-ubah-indikator-sub-sub-kegiatan', 'MasterSubSubKegiatanController@prosesUbahIndikator');

    // Pengguna
    Route::get('/pengguna', 'PenggunaController@index');
    Route::get('/tambah-pengguna', 'PenggunaController@tambah');    
    Route::get('/get-sub-bidang', 'PenggunaController@getSubBid');
    Route::post('/proses-tambah-pengguna', 'PenggunaController@prosesTambahPengguna');
    Route::get('/ubah-pengguna/{id}', 'PenggunaController@ubah');
    Route::post('/proses-ubah-pengguna', 'PenggunaController@prosesUbahPengguna');
    Route::get('/proses-aktifasi-pengguna/{id}', 'PenggunaController@prosesAktifasiPengguna');

    //Sub Bidang
    Route::get('/sub-bidang', 'SubBidangController@index');
    Route::get('/tambah-sub-bidang', 'SubBidangController@tambah');    
    Route::get('/ubah-sub-bidang/{id}', 'SubBidangController@ubah');  
    Route::post('/proses-tambah-sub-bidang', 'SubBidangController@prosesTambah');  
    Route::post('/proses-ubah-sub-bidang', 'SubBidangController@prosesUbah');  

    //Mastering Sub Kegiatan
    // Route::get('/mastering-program', 'MasteringProgramController@index');
    // Route::get('/ajax-program', 'MasteringProgramController@programAjax');
    // Route::post('/proses-tambah-program', 'MasteringProgramController@prosesTambah');
    
    // // Route::post('/proses-tambah-sub-kegiatan', 'MasteringProgramController@prosesTambahSubKegiatan');
    // Route::post('/proses-tambah-sub-bidang-kegiatan', 'MasteringProgramController@prosesTambahSubBidangKegiatan');
    // Route::post('/proses-tambah-indikator-program', 'MasteringProgramController@prosesTambahIndikatirProgram');
    // Route::post('/proses-tambah-indikator-kegiatan', 'MasteringProgramController@prosesTambahIndikatirKegiatan');
    // Route::post('/proses-tambah-indikator-subkegiatan', 'MasteringProgramController@prosesTambahIndikatirSubKegiatan');
    // Route::post('/proses-tambah-indikator-subbid', 'MasteringProgramController@prosesTambahIndikatirSubBid');
    // Route::post('/proses-tambah-target-program', 'MasteringProgramController@prosesTambahTargetProgram');
    // Route::post('/proses-tambah-target-kegiatan', 'MasteringProgramController@prosesTambahTargetKegiatan');
    // Route::post('/proses-tambah-target-sub-kegiatan', 'MasteringProgramController@prosesTambahTargetSubKegiatan');
    // Route::post('/proses-tambah-target-sub-bidang', 'MasteringProgramController@prosesTambahTargetSubBidang');
    // Route::post('/proses-set-pagu', 'MasteringProgramController@prosesAturPagu');

    //Realisasi Anggaran
    Route::get('/realisasi-anggaran', 'RealisasiAnggaranController@index');
    Route::post('/proses-realisasi', 'RealisasiAnggaranController@prosesRealisasi');
    Route::post('/proses-realisasi-ubah', 'RealisasiAnggaranController@prosesRealisasiUbah');

    //Laporan
    Route::get('/laporan-kegiatan', 'LaporanController@laporanKegiatan');
    Route::get('/laporan-sub-bidang', 'LaporanController@laporanSubBidang');
});